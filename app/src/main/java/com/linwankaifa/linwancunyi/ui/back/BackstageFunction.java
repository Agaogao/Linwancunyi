package com.linwankaifa.linwancunyi.ui.back;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;

import com.linwankaifa.linwancunyi.R;
import com.linwankaifa.linwancunyi.ui.login.LoginChooseCharcater;

/**
 * Created by 余梦麒 on 2019/7/14.
 */

public class BackstageFunction extends AppCompatActivity {
    String SHAREDPREFERENCES_NAME = "shared.xml";
    SharedPreferences preferences;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_backstage_function);

        preferences = getSharedPreferences(SHAREDPREFERENCES_NAME, Context.MODE_PRIVATE);

    }

    public void patient_list(View view) {
        Intent intent = new Intent(BackstageFunction.this, BackPatientList.class);
        startActivity(intent);
    }

    public void doctor_list(View view) {
        Intent intent = new Intent(BackstageFunction.this, BackDoctorList.class);
        startActivity(intent);
    }

    public void add_topic(View view) {
        Intent intent = new Intent(BackstageFunction.this, BackAddHuati.class);
        startActivity(intent);
    }

    public void add_hospital(View view) {
        Intent intent = new Intent(BackstageFunction.this, BackAddHospital.class);
        startActivity(intent);
    }

    public void statistics(View view) {
        Intent intent = new Intent(BackstageFunction.this, BackStatisticalInformation.class);
        startActivity(intent);
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {


        if (keyCode == KeyEvent.KEYCODE_BACK) {

            Intent home = new Intent(Intent.ACTION_MAIN);

            home.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            home.addCategory(Intent.CATEGORY_HOME);

            startActivity(home);

            return true;

        }

        return super.onKeyDown(keyCode, event);

    }

    public void quit(View view) {
        Intent intent = new Intent(BackstageFunction.this, LoginChooseCharcater.class);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("token", "");
        editor.commit();
        startActivity(intent);

    }
}

package com.linwankaifa.linwancunyi.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.linwankaifa.linwancunyi.R;
import com.linwankaifa.linwancunyi.bean.SymptomaticBean;
import com.linwankaifa.linwancunyi.util.ViewHolderExt;

import java.util.List;

/**
 * Created by Administrator on 2019/7/17.
 */

public class SymptomaticsiteAdapter extends ArrayAdapter<SymptomaticBean> {
    private int resourceId;

    public SymptomaticsiteAdapter(Context context, int textViewResourceId, List<SymptomaticBean> objects) {
        super(context, textViewResourceId, objects);
        resourceId = textViewResourceId;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(resourceId, null);
        }
        SymptomaticBean symptomaticBean = getItem(position);//获取当前项的Msg实例

        ((TextView) ViewHolderExt.get(convertView, R.id.tv_symptomaticsite_name)).setText(symptomaticBean.getSymptomaticsite());

        if (position == 0) {
            convertView.performClick();
        }

        return convertView;
    }
}

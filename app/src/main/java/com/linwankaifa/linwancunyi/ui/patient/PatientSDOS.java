package com.linwankaifa.linwancunyi.ui.patient;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.linwankaifa.linwancunyi.R;
import com.linwankaifa.linwancunyi.bean.SymptomaticBean;
import com.linwankaifa.linwancunyi.ui.adapter.SymptomaticAdapter;
import com.linwankaifa.linwancunyi.ui.adapter.SymptomaticsiteAdapter;
import com.linwankaifa.linwancunyi.util.Data_whole;
import com.linwankaifa.linwancunyi.util.JsonTransformation;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class PatientSDOS extends AppCompatActivity {
    private ListView symptomaticListView;
    private ListView symptomaticsiteListView;
    private SymptomaticAdapter symptomaticAdapter;
    private SymptomaticsiteAdapter symptomaticsiteAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patient_sdos);
        OkHttpClient client = new OkHttpClient();
        final SymptomaticBean symptomatic = new SymptomaticBean();

        String url = getString(R.string.url)+"/patient/getSymptomaticlist";
        Request request = new Request.Builder()
                .url(url)
                .get()
                .build();
        okhttp3.Call call = client.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Toast toast = Toast.makeText(getApplicationContext(), "症状信息获取失败", Toast.LENGTH_SHORT);
                toast.show();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                // 判断是否成功
                final String isSuccess = response.body().string();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (isSuccess.equals("404")) {
                            Toast.makeText(getApplicationContext(), "该症状不存在", Toast.LENGTH_SHORT).show();
                        } else {
                            final List<SymptomaticBean> symptomaticlist = JsonTransformation.jsonToList(isSuccess, SymptomaticBean.class);
                            final List<SymptomaticBean> symptomaticsitelist = new ArrayList<>();
                            Set<String> set = new HashSet<>();
                            for (int i = 0; i < symptomaticlist.size(); i++) {
                                if (!set.contains(symptomaticlist.get(i).getSymptomaticsite())) {
                                    set.add(symptomaticlist.get(i).getSymptomaticsite());
                                    symptomaticsitelist.add(symptomaticlist.get(i));
                                }
                            }
                            symptomaticsiteAdapter = new SymptomaticsiteAdapter(PatientSDOS.this, R.layout.item_symptomaticsite, symptomaticsitelist);
                            symptomaticsiteListView = (ListView) findViewById(R.id.ct_symptomaticsite_list);
                            symptomaticsiteListView.setAdapter(symptomaticsiteAdapter);
                            symptomaticsiteListView.setItemChecked(0, true);
                            symptomaticsiteListView.post(new Runnable() {
                                @Override
                                public void run() {
                                    symptomaticsiteListView.setItemChecked(0, true);
                                }
                            });
                            SymptomaticBean symptomatic = symptomaticsitelist.get(0);
                            final List<SymptomaticBean> list = new ArrayList<>();
                            for (int i = 0; i < symptomaticlist.size(); i++) {
                                if (symptomaticlist.get(i).getSymptomaticsite().equals(symptomatic.getSymptomaticsite())) {
                                    list.add(symptomaticlist.get(i));
                                }
                            }
                            symptomaticAdapter = new SymptomaticAdapter(PatientSDOS.this, R.layout.item_symptomatic, list);
                            symptomaticListView = (ListView) findViewById(R.id.ct_symptomatic_list);
                            symptomaticListView.setAdapter(symptomaticAdapter);
                            symptomaticListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                @Override
                                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                                    SymptomaticBean symptomatic = list.get(position);
                                    Data_whole.setSymptomaticBean(symptomatic);
                                    Log.e("dynamic", symptomatic.getSymptomatic());
                                    Intent intent = new Intent(PatientSDOS.this, PatientSDOSResult.class);
                                    startActivity(intent);
                                }
                            });
                            symptomaticsiteListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                @Override
                                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                    SymptomaticBean symptomatic = symptomaticsitelist.get(position);
                                    final List<SymptomaticBean> list = new ArrayList<>();
                                    for (int i = 0; i < symptomaticlist.size(); i++) {
                                        if (symptomaticlist.get(i).getSymptomaticsite().equals(symptomatic.getSymptomaticsite())) {
                                            list.add(symptomaticlist.get(i));
                                        }
                                    }
                                    symptomaticAdapter = new SymptomaticAdapter(PatientSDOS.this, R.layout.item_symptomatic, list);
                                    symptomaticListView = (ListView) findViewById(R.id.ct_symptomatic_list);
                                    symptomaticListView.setAdapter(symptomaticAdapter);

                                    symptomaticListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                        @Override
                                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                                            SymptomaticBean symptomatic = list.get(position);
                                            Data_whole.setSymptomaticBean(symptomatic);
                                            Log.e("dynamic", symptomatic.getSymptomatic());
                                            Intent intent = new Intent(PatientSDOS.this, PatientSDOSResult.class);
                                            startActivity(intent);
                                        }
                                    });
                                }
                            });

                        }
                    }
                });
            }
        });
    }

    public void back(View view) {
        this.finish();
    }
}

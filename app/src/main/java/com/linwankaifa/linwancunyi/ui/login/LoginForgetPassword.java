package com.linwankaifa.linwancunyi.ui.login;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.linwankaifa.linwancunyi.R;
import com.linwankaifa.linwancunyi.bean.DoctorBean;
import com.linwankaifa.linwancunyi.bean.ManagerBean;
import com.linwankaifa.linwancunyi.bean.PatientBean;
import com.linwankaifa.linwancunyi.util.JsonTransformation;
import com.linwankaifa.linwancunyi.util.Md5Util;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class LoginForgetPassword extends AppCompatActivity {
    EditText forget_accountnumber;
    EditText forget_password;
    EditText check_password;
    String SHAREDPREFERENCES_NAME = "shared.xml";
    SharedPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgetpassword);

        preferences = getSharedPreferences(SHAREDPREFERENCES_NAME, Context.MODE_PRIVATE);
        forget_accountnumber = (EditText) findViewById(R.id.forget_accountnumber);
        forget_password = (EditText) findViewById(R.id.forget_password);
        check_password = (EditText) findViewById(R.id.check_password);
    }

    public void forgetpasswordbtn(View view) {
        String accountnumber = forget_accountnumber.getText().toString();
        String password = forget_password.getText().toString();
        String checkpassword = check_password.getText().toString();
        if (accountnumber.equals("") || password.equals("") || checkpassword.equals("")) {
            Toast.makeText(getApplicationContext(), "输入不能为空", Toast.LENGTH_SHORT).show();
        } else {
            if (password.equals(checkpassword)) {
                OkHttpClient client = new OkHttpClient();
                RequestBody requestBody = null;
                password = Md5Util.StringInMd5(password);
                switch (preferences.getString("usertype", "")) {
                    case "patient":
                        PatientBean patient = new PatientBean();
                        patient.setAccountnumber(accountnumber);
                        patient.setPassword(password);
                        requestBody = RequestBody.create(MediaType.parse("text; charset=utf-8"),
                                JsonTransformation.objectToJson(patient));
                        Log.e("dynamic", JsonTransformation.objectToJson(patient));
                        break;
                    case "doctor":
                        DoctorBean doctor = new DoctorBean();
                        doctor.setDoctoraccountnumber(accountnumber);
                        doctor.setDoctorpassword(password);
                        requestBody = RequestBody.create(MediaType.parse("text; charset=utf-8"),
                                JsonTransformation.objectToJson(doctor));
                        Log.e("dynamic", JsonTransformation.objectToJson(doctor));
                        break;
                    case "back":
                        ManagerBean managerBean = new ManagerBean();
                        managerBean.setManageraccountnumber(accountnumber);
                        managerBean.setManagerpassword(password);
                        requestBody = RequestBody.create(MediaType.parse("text; charset=utf-8"),
                                JsonTransformation.objectToJson(managerBean));
                        break;
                    default:
                        Toast.makeText(getApplicationContext(), "不正确的角色", Toast.LENGTH_SHORT).show();
                        break;

                }

                String url = getString(R.string.url)+"/";
                url += preferences.getString("usertype", "");
                url += "/forgetpassword";
                Log.e("dynamic", url);
                Request request = new Request.Builder()
                        .url(url)
                        .post(requestBody)
                        .build();
                okhttp3.Call call = client.newCall(request);
                call.enqueue(new Callback() {
                    @Override
                    public void onFailure(Call call, IOException e) {
                        Toast toast = Toast.makeText(getApplicationContext(), "密码重置失败", Toast.LENGTH_SHORT);
                        toast.show();
                    }

                    @Override
                    public void onResponse(Call call, Response response) throws IOException {
                        // 判断是否成功
                        final String isSuccess = response.body().string();
                        Log.e("dz",isSuccess);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                switch (isSuccess) {
                                    case "404":
                                        Toast.makeText(getApplicationContext(), "密码重置失败", Toast.LENGTH_SHORT).show();
                                        break;
                                    case "200":
                                        LoginForgetPassword.this.finish();
                                        Toast.makeText(getApplicationContext(), "密码重置成功", Toast.LENGTH_SHORT).show();
                                        break;
                                    default:
                                        Toast.makeText(getApplicationContext(), "账号错误", Toast.LENGTH_SHORT).show();
                                        break;
                                }
                            }
                        });
                    }
                });

            } else {
                Toast.makeText(getApplicationContext(), "两次密码输入不一致", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void return_log(View view) {
        this.finish();
    }
}

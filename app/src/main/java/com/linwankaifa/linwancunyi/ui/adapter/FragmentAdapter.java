package com.linwankaifa.linwancunyi.ui.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.ArrayList;

/**
 * Created by 85435 on 2019/7/11 0011.
 */

public class FragmentAdapter extends FragmentStatePagerAdapter {

    ArrayList<Fragment> list;

    //通过构造获取list集合
    public FragmentAdapter(FragmentManager fm, ArrayList<Fragment> list) {
        super(fm);
        this.list = list;
    }

    //设置每一个的内容
    @Override
    public Fragment getItem(int arg0) {
        // TODO Auto-generated method stub
        return list.get(arg0);
    }

    //设置有多少内容
    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return list.size();
    }

}
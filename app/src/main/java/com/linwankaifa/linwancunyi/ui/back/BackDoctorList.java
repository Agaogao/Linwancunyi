package com.linwankaifa.linwancunyi.ui.back;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.linwankaifa.linwancunyi.R;
import com.linwankaifa.linwancunyi.bean.DoctorBean;
import com.linwankaifa.linwancunyi.ui.adapter.DoctorAdapter;
import com.linwankaifa.linwancunyi.util.Data_whole;
import com.linwankaifa.linwancunyi.util.JsonTransformation;

import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static android.widget.Toast.makeText;

public class BackDoctorList extends AppCompatActivity {

    List<DoctorBean> doctors;
    private ListView doctorListView;
    private DoctorAdapter adapter;
    private List<DoctorBean> inflictlist;
    View.OnKeyListener onKey = new View.OnKeyListener() {
        @Override
        public boolean onKey(View v, int keyCode, KeyEvent event) {
            // TODO Auto-generated method stub
            if (event.getAction() == KeyEvent.ACTION_DOWN) {
                if (keyCode == KeyEvent.KEYCODE_DPAD_CENTER
                        || keyCode == KeyEvent.KEYCODE_ENTER) {

                    inflictlist = new ArrayList<>();
                    DoctorBean doctorBeantest = new DoctorBean();
                    Field[] fields = doctorBeantest.getClass().getDeclaredFields();

                    for (DoctorBean doctorBean : doctors) {
                        for (Field field : fields) {
                            try {
                                String name = field.getName();
                                name = field.getName().substring(0, 1).toUpperCase() + name.substring(1);
                                Method m = doctorBean.getClass().getMethod("get" + name);
                                String value = String.valueOf(m.invoke(doctorBean));
                                Log.e("name", String.valueOf(field.getName()));

                                Log.e("value", String.valueOf(value));
                                if (value != null && value.contains(((EditText) findViewById(R.id.doctorlistsearch)).getText().toString())) {
                                    inflictlist.add(doctorBean);
                                    break;
                                }
                            } catch (Exception e) {
                                continue;
                            }

                        }

                    }
                    adapter = new DoctorAdapter(BackDoctorList.this, R.layout.item_doctor, inflictlist);
                    doctorListView = (ListView) findViewById(R.id.back_doctor_list_view);
                    doctorListView.setAdapter(adapter);

                    return true;
                }
            }

            return false;
        }
    };
    private EditText search_content;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doctor_list);

        initList();
        search_content = (EditText) findViewById(R.id.doctorlistsearch);
        search_content.setOnKeyListener(onKey);
    }

    public void back(View view) {
        this.finish();
    }

    private void initList() {
        OkHttpClient client = new OkHttpClient();
        RequestBody requestBody = null;

        String url = getString(R.string.url)+"/";
        url += "patient/getdoctorinfolist";

        Request request = new Request.Builder()
                .url(url)
                .build();
        okhttp3.Call call = client.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                // 如果失败，就显示失败信息
                Toast toast = makeText(getApplicationContext(), "请求失败", Toast.LENGTH_SHORT);
                toast.show();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                // 判断请求是否成功
                final String isSuccess = response.body().string();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (isSuccess.equals("404"))
                            makeText(getApplicationContext(), "错误请求404", Toast.LENGTH_SHORT).show();
                        else {
                            doctors = JsonTransformation.jsonToList(isSuccess, DoctorBean.class);
                            inflictlist = doctors;
                            adapter = new DoctorAdapter(BackDoctorList.this, R.layout.item_doctor, inflictlist);
                            doctorListView = (ListView) findViewById(R.id.back_doctor_list_view);
                            doctorListView.setAdapter(adapter);

                            doctorListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                @Override
                                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                                    DoctorBean doctor = inflictlist.get(i);
                                    Data_whole.setDoctorBean(doctor);
                                    Intent intent = new Intent(BackDoctorList.this, BackDoctorInfo.class);
                                    startActivity(intent);
                                }
                            });
                        }
                    }
                });
            }
        });
    }
}

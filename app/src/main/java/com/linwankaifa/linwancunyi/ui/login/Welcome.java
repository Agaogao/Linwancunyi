package com.linwankaifa.linwancunyi.ui.login;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.linwankaifa.linwancunyi.R;
import com.linwankaifa.linwancunyi.bean.DoctorBean;
import com.linwankaifa.linwancunyi.bean.ManagerBean;
import com.linwankaifa.linwancunyi.bean.PatientBean;
import com.linwankaifa.linwancunyi.ui.back.BackstageFunction;
import com.linwankaifa.linwancunyi.ui.doctor.DoctorFunction;
import com.linwankaifa.linwancunyi.ui.patient.PatientMain;
import com.linwankaifa.linwancunyi.util.Data_whole;
import com.linwankaifa.linwancunyi.util.JsonTransformation;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class Welcome extends AppCompatActivity {
    String SHAREDPREFERENCES_NAME = "shared.xml";
    SharedPreferences preferences;
    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            judgeToken();
            super.handleMessage(msg);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        preferences = getSharedPreferences(SHAREDPREFERENCES_NAME, Context.MODE_PRIVATE);
        handler.sendEmptyMessageDelayed(0, 1000);

    }

    private void judgeToken() {

        // 获取编辑框中的值
        String token = preferences.getString("token", "");
        if (token.isEmpty()) {
            turntochoose();
            return;
        }

        OkHttpClient client = new OkHttpClient();
        // 根据不同的角色构建不同的url
        String url = getString(R.string.url)+"/";
        url += preferences.getString("usertype", "");
        url += "/checktoken";
        Request request = new Request.Builder()
                .url(url)
                .addHeader("token", token)
                .build();
        okhttp3.Call call = client.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                // 如果失败，就显示失败信息
                return;
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                // 判断请求是否成功
                final String isSuccess = response.body().string();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        switch (isSuccess) {
                            case "404":
                                turntochoose();
                                return;
                            default:
                                switch (preferences.getString("usertype", "")) {
                                    case "patient":
                                        // 创建一个患者的实体类，并将其值修改为需要查询的值
                                        Data_whole.setPatientBean(JsonTransformation.jsonToObject(isSuccess, PatientBean.class));
                                        Intent intent1 = new Intent(Welcome.this, PatientMain.class);
                                        startActivity(intent1);
                                        break;
                                    case "doctor":
                                        Data_whole.setDoctorBean(JsonTransformation.jsonToObject(isSuccess, DoctorBean.class));
                                        Intent intent2 = new Intent(Welcome.this, DoctorFunction.class);
                                        startActivity(intent2);
                                        break;
                                    case "back":
                                        Data_whole.setManagerBean(JsonTransformation.jsonToObject(isSuccess, ManagerBean.class));
                                        Intent intent3 = new Intent(Welcome.this, BackstageFunction.class);
                                        startActivity(intent3);
                                        break;
                                    default:
                                        // 如果是其他角色，那么应该是出错了,直接return
                                        Toast.makeText(getApplicationContext(), "不正确的角色", Toast.LENGTH_SHORT).show();
                                        turntochoose();
                                        return;
                                }
                        }
                    }
                });
            }
        });
    }

    private void turntochoose() {
        Intent intent = new Intent(Welcome.this, LoginChooseCharcater.class);
        startActivity(intent);
    }

}

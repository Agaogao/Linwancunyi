package com.linwankaifa.linwancunyi.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.linwankaifa.linwancunyi.R;
import com.linwankaifa.linwancunyi.bean.HuatiBean;
import com.linwankaifa.linwancunyi.util.ViewHolderExt;

import java.util.List;

/**
 * Created by 邹京汕 on 2019/7/16.
 */

public class HuatiAdapter extends ArrayAdapter<HuatiBean> {
    private int resourceId;

    public HuatiAdapter(Context context, int textViewResourceId, List<HuatiBean> objects) {
        super(context, textViewResourceId, objects);
        resourceId = textViewResourceId;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(resourceId, null);
        }
        HuatiBean huatiBean = getItem(position);//获取当前项的Msg实例

        ((TextView) ViewHolderExt.get(convertView, R.id.tv_huati_title)).setText(huatiBean.getBriefintroduction());
        ((TextView) ViewHolderExt.get(convertView, R.id.tv_huati_date)).setText(huatiBean.getFabudate());
        ((TextView) ViewHolderExt.get(convertView, R.id.tv_huati_content)).setText(huatiBean.getFabucontent());

        return convertView;
    }
}
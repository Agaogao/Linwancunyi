package com.linwankaifa.linwancunyi.ui.patient;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.linwankaifa.linwancunyi.R;
import com.linwankaifa.linwancunyi.bean.ChatrecordBean;
import com.linwankaifa.linwancunyi.ui.adapter.PatientMsgAdapter;
import com.linwankaifa.linwancunyi.util.Data_whole;
import com.linwankaifa.linwancunyi.util.JsonTransformation;

import java.io.IOException;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by 余梦麒 on 2019/7/12.
 */


public class PatientChat extends AppCompatActivity {
    // 声明preferences
    String SHAREDPREFERENCES_NAME = "shared.xml";
    SharedPreferences preferences;
    ChatrecordBean chatrecordBean;
    SharedPreferences.Editor editor;
    int chatnumber;
    Timer timer;
    TimerTask timerTask;
    EditText editText;
    TextView chat_doctor_name;
    TextView chat_doctor_sex;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patient_chat);

        chat_doctor_name = (TextView) findViewById(R.id.chat_doctor_name);
        chat_doctor_sex = (TextView) findViewById(R.id.chat_doctor_sex);
        chat_doctor_name.setText(Data_whole.getDoctorBean().getDoctorname());
        chat_doctor_sex.setText(Data_whole.getDoctorBean().getDoctorsex());

        preferences = getSharedPreferences(SHAREDPREFERENCES_NAME, Context.MODE_PRIVATE);

        chatnumber = 0;
        editText = (EditText) findViewById(R.id.patient_chat_edit);
        init();
        initTimer();

        editText.setText(preferences.getString("question_content", ""));

        editor = preferences.edit();

        editor.putString("question_content", "");
        editor.commit();

    }

    @Override
    protected void onPause() {
        super.onPause();
        timer.cancel();
    }

    @Override
    protected void onResume() {
        super.onResume();
        initTimer();
    }

    private void initTimer() {


        timer = new Timer();
        timerTask = new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        init();

                    }
                });
            }
        };
        timer.schedule(timerTask, 0, 5000);

    }


    private void init() {
        OkHttpClient client = new OkHttpClient();
        RequestBody requestBody = null;


        chatrecordBean = new ChatrecordBean();
        Log.e("patient1", Data_whole.getPatientBean().toString());

        chatrecordBean.setDoctorid(Data_whole.getDoctorBean().getDoctorid());
        chatrecordBean.setPatientid(Data_whole.getPatientBean().getPatientid());

        requestBody = RequestBody.create(MediaType.parse("text; charset=utf-8"),
                JsonTransformation.objectToJson(chatrecordBean));

        // 根据不同的角色构建不同的url
        String url = getString(R.string.url)+"/";
        url += preferences.getString("usertype", "");
        url += "/getchatinfo";

        // 构建请求体，并执行
        Request request = new Request.Builder()
                .url(url)
                .post(requestBody)
                .build();
        okhttp3.Call call = client.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                // 如果失败，就显示失败信息
                Toast toast = Toast.makeText(getApplicationContext(), "登录失败", Toast.LENGTH_SHORT);
                toast.show();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                // 判断请求是否成功
                final String isSuccess = response.body().string();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        switch (isSuccess) {
                            case "404":
                                Toast.makeText(getApplicationContext(), "查询错误", Toast.LENGTH_SHORT).show();
                                break;
                            case "200":
                                // 如果成功过则进行业务逻辑操作
                                Toast.makeText(getApplicationContext(), "获取聊天错误", Toast.LENGTH_SHORT).show();
                                break;
                            default:
                                List<ChatrecordBean> chatrecordlist = JsonTransformation.jsonToList(isSuccess, ChatrecordBean.class);
                                int number = chatrecordlist.size();
                                if (number != chatnumber) {
                                    ListView listView = (ListView) findViewById(R.id.patient_chat_list_view);
                                    listView.setAdapter(new PatientMsgAdapter(PatientChat.this, R.layout.list_msg_layout, chatrecordlist));
                                    chatnumber = number;
                                }
                                break;
                        }
                    }
                });
            }
        });
    }

    public void send(View view) {
        OkHttpClient client = new OkHttpClient();
        RequestBody requestBody = null;


        ChatrecordBean newchat = new ChatrecordBean();

        newchat.setDoctorid(Data_whole.getDoctorBean().getDoctorid());
        Log.e("patient", Data_whole.getPatientBean().toString());
        newchat.setPatientid(Data_whole.getPatientBean().getPatientid());

        switch (preferences.getString("usertype", "")) {
            case "patient":
                newchat.setIspatienttodoctor(1);
                break;
            case "doctor":
                newchat.setIspatienttodoctor(0);
            default:
                newchat.setIspatienttodoctor(0);
                break;
        }


        newchat.setChatcontent(editText.getText().toString());
        switch (preferences.getString("usertype", "")) {
            case "patient":
                newchat.setIspatienttodoctor(1);
                break;
            case "doctor":
                newchat.setIspatienttodoctor(0);
                break;
        }

        requestBody = RequestBody.create(MediaType.parse("text; charset=utf-8"),
                JsonTransformation.objectToJson(newchat));

        // 根据不同的角色构建不同的url
        String url = getString(R.string.url)+"/";
        url += preferences.getString("usertype", "");
        url += "/chat";

        // 构建请求体，并执行
        Request request = new Request.Builder()
                .url(url)
                .post(requestBody)
                .build();
        okhttp3.Call call = client.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                // 如果失败，就显示失败信息
                Toast toast = Toast.makeText(getApplicationContext(), "登录失败", Toast.LENGTH_SHORT);
                toast.show();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                // 判断请求是否成功
                final String isSuccess = response.body().string();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        switch (isSuccess) {
                            case "404":
                                Toast.makeText(getApplicationContext(), "无法获取记录", Toast.LENGTH_SHORT).show();
                                break;
                            default:
                                List<ChatrecordBean> chatrecordlist = JsonTransformation.jsonToList(isSuccess, ChatrecordBean.class);
                                int number = chatrecordlist.size();
                                if (number != chatnumber) {
                                    ListView listView = (ListView) findViewById(R.id.patient_chat_list_view);
                                    listView.setAdapter(new PatientMsgAdapter(PatientChat.this, R.layout.list_msg_layout, chatrecordlist));
                                    chatnumber = number;
                                    editText.setText("");
                                }
                                break;
                        }
                    }
                });
            }
        });

    }

    public void back(View view) {

        Intent intent = new Intent(PatientChat.this, PatientMain.class);
        startActivity(intent);
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {


        if (keyCode == KeyEvent.KEYCODE_BACK) {

            Intent intent = new Intent(PatientChat.this, PatientMain.class);
            startActivity(intent);
            return true;

        }

        return super.onKeyDown(keyCode, event);

    }
}

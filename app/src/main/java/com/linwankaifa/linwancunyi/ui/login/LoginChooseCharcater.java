package com.linwankaifa.linwancunyi.ui.login;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;

import com.linwankaifa.linwancunyi.R;

public class LoginChooseCharcater extends AppCompatActivity {
    String SHAREDPREFERENCES_NAME = "shared.xml";
    SharedPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choosecharcater);

        preferences = getSharedPreferences(SHAREDPREFERENCES_NAME, Context.MODE_PRIVATE);
    }


    public void choose_patient(View view) {
        turntopatient();
    }

    private void turntopatient() {
        Intent intent = new Intent(LoginChooseCharcater.this, Login.class);
        intent.putExtra("choose", "patient");
        startActivity(intent);

        // 将用户的类别更改为患者，存在本地，一下几个作用相同
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("usertype", "patient");
        editor.commit();
    }

    public void choose_doctor(View view) {
        turntodoctor();
    }

    private void turntodoctor() {
        Intent intent = new Intent(LoginChooseCharcater.this, Login.class);
        intent.putExtra("choose", "doctor");
        startActivity(intent);

        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("usertype", "doctor");
        editor.commit();
    }

    public void choose_backstage(View view) {
        turntoback();
    }

    private void turntoback() {
        Intent intent = new Intent(LoginChooseCharcater.this, Login.class);
        intent.putExtra("choose", "backstage");


        startActivity(intent);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("usertype", "back");
        editor.commit();
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {


        if (keyCode == KeyEvent.KEYCODE_BACK) {

            Intent home = new Intent(Intent.ACTION_MAIN);

            home.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            home.addCategory(Intent.CATEGORY_HOME);

            startActivity(home);

            return true;

        }

        return super.onKeyDown(keyCode, event);

    }
}

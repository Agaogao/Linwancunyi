package com.linwankaifa.linwancunyi.ui.patient;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.linwankaifa.linwancunyi.R;
import com.linwankaifa.linwancunyi.bean.PatientBean;
import com.linwankaifa.linwancunyi.util.Data_whole;
import com.linwankaifa.linwancunyi.util.JsonTransformation;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class PatientHealthCard extends AppCompatActivity {

    TextView name;
    TextView gender_age;

    TextView tv_hunying;
    TextView tv_guomingshi;
    EditText tv_shengao;
    EditText tv_tizhong;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_patient_health_card);

        tv_shengao = (EditText) findViewById(R.id.tv_shengao);
        tv_tizhong = (EditText) findViewById(R.id.tv_tizhong);
        tv_hunying = (TextView) findViewById(R.id.tv_hunying);
        tv_guomingshi = (TextView) findViewById(R.id.tv_guomingshi);
        name = (TextView) findViewById(R.id.name);
        gender_age = (TextView) findViewById(R.id.gender_age);

        initialization();
    }

    protected void dialog() {
        Dialog dialog = new AlertDialog.Builder(this).setIcon(
                android.R.drawable.btn_star).setTitle("婚姻状况").setMessage(
                "请选择？").setPositiveButton("已婚",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub
                        tv_hunying.setText("已婚");
                    }
                }).setNegativeButton("未婚", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // TODO Auto-generated method stub
                tv_hunying.setText("未婚");
            }
        }).setNeutralButton("取消", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // TODO Auto-generated method stub

            }
        }).create();
        dialog.show();
    }

    protected void dialog1() {
        Dialog dialog = new AlertDialog.Builder(this).setIcon(
                android.R.drawable.btn_star).setTitle("是否有过敏史").setMessage(
                "请选择？").setPositiveButton("是",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub
                        tv_guomingshi.setText("是");
                    }
                }).setNegativeButton("否", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // TODO Auto-generated method stub
                tv_guomingshi.setText("否");
            }
        }).setNeutralButton("取消", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // TODO Auto-generated method stub

            }
        }).create();
        dialog.show();
    }

    public void marriage(View view) {
        dialog();
    }

    public void allergy(View view) {
        dialog1();
    }

    public void conserve(View view) {
        update();
    }

    public void update() {
        Log.e("dd",tv_shengao.getText().toString());
        if(tv_shengao.getText().toString().equals("") || tv_tizhong.getText().toString().equals(""))
        {
            Toast.makeText(PatientHealthCard.this, "输入不能为空", Toast.LENGTH_SHORT).show();
        }
        else {
            OkHttpClient client = new OkHttpClient();
            String url = getString(R.string.url)+"/patient/updatepatientinfo";

            RequestBody requestBody = null;
            PatientBean patientBean = Data_whole.getPatientBean();
            patientBean.setHeight(Integer.parseInt(tv_shengao.getText().toString()));
            patientBean.setWeight(Integer.parseInt(tv_tizhong.getText().toString()));
            patientBean.setIsmarriage(tv_hunying.getText().toString());
            patientBean.setIsallergy(tv_guomingshi.getText().toString());

            Data_whole.setPatientBean(patientBean);

            requestBody = RequestBody.create(MediaType.parse("text; charset=utf-8"),
                    JsonTransformation.objectToJson(patientBean));

            Request request = new Request.Builder()
                    .url(url)
                    .post(requestBody)
                    .build();
            okhttp3.Call call = client.newCall(request);
            call.enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    Toast toast = Toast.makeText(getApplicationContext(), "保存失败", Toast.LENGTH_SHORT);
                    toast.show();
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    // 判断是否成功
                    final String isSuccess = response.body().string();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (isSuccess.equals("404"))
                                Toast.makeText(PatientHealthCard.this, "404", Toast.LENGTH_SHORT).show();
                            if (isSuccess.equals("200")) {
                                Toast.makeText(PatientHealthCard.this, "保存成功", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                }
            });
        }
    }

    public void initialization() {
        tv_shengao.setText(String.valueOf(Data_whole.getPatientBean().getHeight()));
        tv_tizhong.setText(String.valueOf(Data_whole.getPatientBean().getWeight()));
        tv_hunying.setText(Data_whole.getPatientBean().getIsmarriage());
        tv_guomingshi.setText(Data_whole.getPatientBean().getIsallergy());
        name.setText(Data_whole.getPatientBean().getPatientname());
        gender_age.setText(Data_whole.getPatientBean().getPatientsex() + " " + Data_whole.getPatientBean().getPatientage() + "岁");
    }

    public void back(View view) {
        this.finish();

    }
}

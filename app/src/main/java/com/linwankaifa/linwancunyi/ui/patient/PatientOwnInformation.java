package com.linwankaifa.linwancunyi.ui.patient;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.linwankaifa.linwancunyi.R;
import com.linwankaifa.linwancunyi.bean.PatientBean;
import com.linwankaifa.linwancunyi.util.Data_whole;

/**
 * Created by 余梦麒 on 2019/7/15.
 */

public class PatientOwnInformation extends AppCompatActivity {
    TextView patientname;
    TextView patientage;
    TextView patientsex;
    TextView patientpetname;
    TextView patientbirthday;
    TextView patientphone;
    TextView patientaccountnumber;
    String SHAREDPREFERENCES_NAME = "shared.xml";
    SharedPreferences preferences;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patient_information);
        preferences = getSharedPreferences(SHAREDPREFERENCES_NAME, Context.MODE_PRIVATE);
        patientname = (TextView) findViewById(R.id.patient_name);
        patientaccountnumber = (TextView) findViewById(R.id.patient_accountnumber);
        patientage = (TextView) findViewById(R.id.patient_age);
        patientsex = (TextView) findViewById((R.id.patient_sex));
        patientbirthday = (TextView) findViewById(R.id.patient_birthday);
        patientphone = (TextView) findViewById(R.id.patient_phone);
        patientpetname = (TextView) findViewById(R.id.patient_petname);
        PatientBean patientBean = Data_whole.getPatientBean();
        if (patientBean.getPatientname() == null) {
            patientBean.setPatientname("新用户");
        }
        if (patientBean.getPetname() == null) {
            patientBean.setPetname("");
        }
        if (patientBean.getPatientage() == null) {
            patientBean.setPatientage(20);
        }
        if (patientBean.getPatientsex() == null) {
            patientBean.setPatientsex("");
        }
        if (patientBean.getPhone() == null) {
            patientBean.setPhone("");
        }
        if (patientBean.getBirthday() == null) {
            patientBean.setBirthday("");
        }
        patientaccountnumber.setText(patientBean.getAccountnumber());
        patientname.setText(patientBean.getPatientname());
        patientpetname.setText(patientBean.getPetname());
        patientage.setText(String.valueOf(patientBean.getPatientage()));
        patientsex.setText(patientBean.getPatientsex());
        patientphone.setText(patientBean.getPhone());
        patientbirthday.setText(patientBean.getBirthday());
    }

    public void patient_change_nickname(View view) {
        Intent intent = new Intent(PatientOwnInformation.this, PatientInformationChange.class);
        intent.putExtra("title", "修改患者昵称");
        startActivity(intent);
    }

    public void patient_change_name(View view) {
        Intent intent = new Intent(PatientOwnInformation.this, PatientInformationChange.class);
        intent.putExtra("title", "修改患者姓名");
        startActivity(intent);
    }

    public void patient_change_gender(View view) {
        Intent intent = new Intent(PatientOwnInformation.this, PatientInformationChange.class);
        intent.putExtra("title", "修改患者性别");
        startActivity(intent);
    }

    public void patient_change_birthday(View view) {
        Intent intent = new Intent(PatientOwnInformation.this, PatientInformationChange.class);
        intent.putExtra("title", "修改患者生日");
        startActivity(intent);
    }

    public void patient_change_tel(View view) {
        Intent intent = new Intent(PatientOwnInformation.this, PatientInformationChange.class);
        intent.putExtra("title", "修改患者电话");
        startActivity(intent);
    }

    public void patient_change_age(View view) {
        Intent intent = new Intent(PatientOwnInformation.this, PatientInformationChange.class);
        intent.putExtra("title", "修改患者年龄");
        startActivity(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        PatientBean patientBean = Data_whole.getPatientBean();
        patientaccountnumber.setText(patientBean.getAccountnumber());
        patientname.setText(patientBean.getPatientname());
        patientpetname.setText(patientBean.getPetname());
        patientage.setText(String.valueOf(patientBean.getPatientage()));
        patientsex.setText(patientBean.getPatientsex());
        patientphone.setText(patientBean.getPhone());
        patientbirthday.setText(patientBean.getBirthday());
    }

    public void back(View view) {
        this.finish();
    }
}

package com.linwankaifa.linwancunyi.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.linwankaifa.linwancunyi.R;
import com.linwankaifa.linwancunyi.bean.DoctorBean;
import com.linwankaifa.linwancunyi.util.ViewHolderExt;

import java.util.List;

/**
 * Created by 邹京汕 on 2019/7/17.
 */

public class ChatDoctorAdapter extends ArrayAdapter<DoctorBean> {

    private int resourceId;

    public ChatDoctorAdapter(Context context, int textViewResourceId, List<DoctorBean> objects) {
        super(context, textViewResourceId, objects);
        resourceId = textViewResourceId;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(resourceId, null);
        }
        DoctorBean doctorBean = getItem(position);//获取当前项的Msg实例

        ((TextView) ViewHolderExt.get(convertView, R.id.tv_chat_doctor_name)).setText(doctorBean.getDoctorname());
        ((TextView) ViewHolderExt.get(convertView, R.id.tv_chat_doctor_brief)).setText(doctorBean.getSpeciality());


        return convertView;
    }
}

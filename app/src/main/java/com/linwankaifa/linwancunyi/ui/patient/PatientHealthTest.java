package com.linwankaifa.linwancunyi.ui.patient;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.linwankaifa.linwancunyi.R;

public class PatientHealthTest extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patient_healthtest);
    }

    public void start_test(View view) {
        Intent intent = new Intent(PatientHealthTest.this, PatientHealthQuestion.class);
        startActivity(intent);
    }

    public void back(View view) {
        this.finish();
    }
}

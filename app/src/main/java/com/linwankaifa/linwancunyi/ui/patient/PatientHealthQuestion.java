package com.linwankaifa.linwancunyi.ui.patient;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.linwankaifa.linwancunyi.R;
import com.linwankaifa.linwancunyi.bean.HealthassessmentResultBean;
import com.linwankaifa.linwancunyi.util.JsonTransformation;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class PatientHealthQuestion extends AppCompatActivity {

    int i = 1;
    int score = 0;
    TextView question;
    Button option1;
    Button option2;
    Button option3;
    Button option4;
    Button next;

    @Override
    protected void onResume() {
        super.onResume();

        score = 0;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patient_healthquestion);

        question = (TextView) findViewById(R.id.question);
        option1 = (Button) findViewById(R.id.option1);
        option2 = (Button) findViewById(R.id.option2);
        option3 = (Button) findViewById(R.id.option3);
        option4 = (Button) findViewById(R.id.option4);

        show(i);
    }

    public void show(int j){
        switch(j){
            case 1:
                question.setText("1.偶尔睡得不好，第二天上班就受不了，总想继续睡。");
                option1.setText("A.没有");
                option2.setText("B.有，但程度浅");
                option3.setText("C.有，但程度深");
                option4.setText("D.有");
                break;
            case 2:
                question.setText("2.出行坐飞机起飞降落时感到耳痛、头痛。");
                option1.setText("A.没有");
                option2.setText("B.有，但程度浅");
                option3.setText("C.有，但程度深");
                option4.setText("D.有");
                break;
            case 3:
                question.setText("3.出行时行走一段路，就觉得腿疼脚痛。");
                option1.setText("A.没有");
                option2.setText("B.有，但程度浅");
                option3.setText("C.有，但程度深");
                option4.setText("D.有");
                break;
            case 4:
                question.setText("4.您是否在服用以下药物。");
                option1.setText("A.没有常服用药");
                option2.setText("B.减压药");
                option3.setText("C.降糖药");
                option4.setText("D.降脂药");
                break;
            case 5:
                question.setText("5.您最近的运动情况。");
                option1.setText("A.每周运动超过三天，已保持6个月以上");
                option2.setText("B.每周运动超过三天，保持少于6个月");
                option3.setText("C.每周运动少于3天");
                option4.setText("D.基本不运动");
                break;
            case 6:
                question.setText("6.您（）一天吃很多种类的食物。");
                option1.setText("A.经常");
                option2.setText("B.有时");
                option3.setText("C.很少");
                option4.setText("D.从不");
                break;
            case 7:
                question.setText("7.您的直系亲属患有以下疾病。");
                option1.setText("A.无以下任何疾病");
                option2.setText("B.糖尿病");
                option3.setText("C.冠心病");
                option4.setText("D.肥胖症");
                break;
            case 8:
                question.setText("8.请问在过去30天，您有多少天精神状况不佳(包括精神紧张、情绪低落和不稳定)。");
                option4.setText("A.大于15天");
                option3.setText("B.7-15天");
                option2.setText("C.1-6天");
                option1.setText("D.无");
                break;
            case 9:
                question.setText("9.在过去的30天内你是否吸过烟？");
                option4.setText("A.每天至少吸一支");
                option3.setText("B.每周至少一支");
                option2.setText("C.每周不足一支");
                option1.setText("D.未吸");
                break;
            case 10:
                question.setText("10.你一般多长时间喝一次酒？");
                option4.setText("A.每天或几乎每天");
                option3.setText("B.每周3-4次");
                option2.setText("C.每周1-2次");
                option1.setText("D.不喝酒");
                break;

        }
    }

    public void send_result(){
        OkHttpClient client = new OkHttpClient();
        String url = getString(R.string.url)+"/patient/getcheckresult";
        RequestBody requestBody = null;
        HealthassessmentResultBean grade = new HealthassessmentResultBean();
        grade.setGrade(score/10*10);
        requestBody = RequestBody.create(MediaType.parse("text; charset=utf-8"),
                JsonTransformation.objectToJson(grade));
        Request request = new Request.Builder()
                .url(url)
                .post(requestBody)
                .build();
        okhttp3.Call call = client.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Toast toast = Toast.makeText(getApplicationContext(), "推送信息失败", Toast.LENGTH_SHORT);
                toast.show();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                // 判断是否成功
                final String isSuccess = response.body().string();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if(isSuccess.equals("404"))
                            Toast.makeText(PatientHealthQuestion.this, "404", Toast.LENGTH_SHORT).show();
                        else
                        {
                            HealthassessmentResultBean result = JsonTransformation.jsonToObject(isSuccess, HealthassessmentResultBean.class);
                            Log.e("aaaaaaaaaaaaaaaaa", String.valueOf(score));
                            Intent intent = new Intent(PatientHealthQuestion.this, PatientHealthResult.class);
                            intent.putExtra("result", result.getResultcontent());
                            startActivity(intent);
                        }
                    }
                });
            }
        });
    }

    public void choose_a(View view) {
        if(i == 10)
            send_result();
        score += 10;
        i++;
        show(i);
    }

    public void choose_b(View view) {
        if(i == 10)
            send_result();
        score += 7;
        i++;
        show(i);
    }

    public void choose_c(View view) {
        if(i == 10)
            send_result();
        score += 3;
        i++;
        show(i);
    }

    public void choose_d(View view) {
        if(i == 10)
            send_result();
        score += 0;
        i++;
        show(i);
    }

    public void back(View view) {
        this.finish();
    }
}

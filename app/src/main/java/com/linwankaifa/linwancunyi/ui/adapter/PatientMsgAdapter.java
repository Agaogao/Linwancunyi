package com.linwankaifa.linwancunyi.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.linwankaifa.linwancunyi.R;
import com.linwankaifa.linwancunyi.bean.ChatrecordBean;
import com.linwankaifa.linwancunyi.util.ViewHolderExt;

import java.util.List;

/**
 * Created by 邹京汕 on 2019/7/15.
 */

public class PatientMsgAdapter extends ArrayAdapter<ChatrecordBean> {
    private int resourceId;

    public PatientMsgAdapter(Context context, int textViewResourceId, List<ChatrecordBean> objects) {
        super(context, textViewResourceId, objects);
        resourceId = textViewResourceId;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(resourceId, null);
        }
        ChatrecordBean chatrecordBean = getItem(position);//获取当前项的Msg实例
        if (chatrecordBean.getIspatienttodoctor() == 0) {
            ViewHolderExt.get(convertView, R.id.left_layout).setVisibility(View.VISIBLE);
            ViewHolderExt.get(convertView, R.id.right_layout).setVisibility(View.GONE);
            ((TextView) ViewHolderExt.get(convertView, R.id.left_msg)).setText(chatrecordBean.getChatcontent());
        } else if (chatrecordBean.getIspatienttodoctor() == 1) {
            ViewHolderExt.get(convertView, R.id.right_layout).setVisibility(View.VISIBLE);
            ViewHolderExt.get(convertView, R.id.left_layout).setVisibility(View.GONE);
            ((TextView) ViewHolderExt.get(convertView, R.id.right_msg)).setText(chatrecordBean.getChatcontent());
        }
        return convertView;
    }
}

package com.linwankaifa.linwancunyi.ui.back;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.linwankaifa.linwancunyi.R;
import com.linwankaifa.linwancunyi.bean.HospitalBean;
import com.linwankaifa.linwancunyi.util.JsonTransformation;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class BackAddHospital extends AppCompatActivity {
    EditText addhospital;
    EditText addplace;
    EditText addjianjie;
    String SHAREDPREFERENCES_NAME = "shared.xml";
    SharedPreferences preferences;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_hospital);
        preferences = getSharedPreferences(SHAREDPREFERENCES_NAME, Context.MODE_PRIVATE);

        addhospital = (EditText) findViewById(R.id.addhospital);
        addplace = (EditText) findViewById(R.id.addplace);
        addjianjie = (EditText) findViewById(R.id.addjianjie);
    }

    public void addhospitalBtn(View view) {
        OkHttpClient client = new OkHttpClient();

        RequestBody requestBody = null;
        String hospital = addhospital.getText().toString();
        String place = addplace.getText().toString();
        String jianjie = addjianjie.getText().toString();
        HospitalBean hospitalen = new HospitalBean();
        hospitalen.setHospitalname(hospital);
        hospitalen.setArea(place);
        hospitalen.setIntroduction(jianjie);
        requestBody = RequestBody.create(MediaType.parse("text; charset=utf-8"),
                JsonTransformation.objectToJson(hospitalen));
        Log.e("dynamic", JsonTransformation.objectToJson(hospitalen));

        String url = getString(R.string.url)+"/back/inserthospital";

        Log.e("dynamic", url);
        Request request = new Request.Builder()
                .url(url)
                .post(requestBody)
                .build();
        okhttp3.Call call = client.newCall(request);

        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Toast toast = Toast.makeText(getApplicationContext(), "返回失败", Toast.LENGTH_SHORT);
                toast.show();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                final String isSuccess = response.body().string();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        switch (isSuccess) {
                            case "404":
                                Toast.makeText(getApplicationContext(), "该医院已存在", Toast.LENGTH_SHORT).show();
                                break;
                            case "200":
                                Toast.makeText(getApplicationContext(), "添加成功", Toast.LENGTH_SHORT).show();
                                break;
                            default:
                                break;
                        }
                    }
                });

            }
        });

    }


    public void back(View view) {
        this.finish();
    }
}


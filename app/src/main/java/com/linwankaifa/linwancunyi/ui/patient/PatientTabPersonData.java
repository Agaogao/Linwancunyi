package com.linwankaifa.linwancunyi.ui.patient;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.linwankaifa.linwancunyi.R;

public class PatientTabPersonData extends AppCompatActivity {
    TextView tv_hunying;
    TextView tv_guomingshi;
    EditText tv_shengao;
    EditText tv_tizhong;
    String SHAREDPREFERENCES_NAME = "shared.xml";
    SharedPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tab_person_data);
        preferences = getSharedPreferences(SHAREDPREFERENCES_NAME, Context.MODE_PRIVATE);

        tv_shengao = (EditText) findViewById(R.id.tv_shengao);
        tv_tizhong = (EditText) findViewById(R.id.tv_tizhong);
        tv_hunying = (TextView) findViewById(R.id.tv_hunying);
        tv_guomingshi = (TextView) findViewById(R.id.tv_guomingshi);
    }

    protected void dialog() {
        Dialog dialog = new AlertDialog.Builder(this).setIcon(
                android.R.drawable.btn_star).setTitle("婚姻状况").setMessage(
                "请选择？").setPositiveButton("已婚",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub
                        tv_hunying.setText("已婚");
                    }
                }).setNegativeButton("未婚", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // TODO Auto-generated method stub
                tv_hunying.setText("未婚");
            }
        }).setNeutralButton("取消", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // TODO Auto-generated method stub

            }
        }).create();
        dialog.show();
    }

    protected void dialog1() {
        Dialog dialog = new AlertDialog.Builder(this).setIcon(
                android.R.drawable.btn_star).setTitle("是否有过敏史").setMessage(
                "请选择？").setPositiveButton("是",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub
                        tv_guomingshi.setText("是");
                    }
                }).setNegativeButton("否", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // TODO Auto-generated method stub
                tv_guomingshi.setText("否");
            }
        }).setNeutralButton("取消", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // TODO Auto-generated method stub

            }
        }).create();
        dialog.show();
    }

    public void hyclick(View view) {
        dialog();
    }

    public void gmsclick(View view) {
        dialog1();
    }
}

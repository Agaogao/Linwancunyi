package com.linwankaifa.linwancunyi.ui.patient;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.linwankaifa.linwancunyi.R;

/**
 * Created by 余梦麒 on 2019/7/15.
 */

public class PatientSetupAndHelp extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setup_and_help);
    }

    public void help(View view) {
        Intent intent = new Intent(PatientSetupAndHelp.this, PatientHelp.class);
        startActivity(intent);
    }

    public void statement(View view) {
        Intent intent = new Intent(PatientSetupAndHelp.this, PatientStatement.class);
        startActivity(intent);
    }

    public void patient_own_information(View view) {
        Intent intent = new Intent(PatientSetupAndHelp.this, PatientOwnInformation.class);
        startActivity(intent);
    }

    public void back(View view) {
        this.finish();
    }
}

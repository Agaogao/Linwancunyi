package com.linwankaifa.linwancunyi.ui.patient;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.linwankaifa.linwancunyi.R;
import com.linwankaifa.linwancunyi.bean.DoctorBean;
import com.linwankaifa.linwancunyi.ui.adapter.ChatDoctorAdapter;
import com.linwankaifa.linwancunyi.util.Data_whole;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link PatientConsult.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link PatientConsult#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PatientConsult extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    View view2;     // TODO: Rename and change types of parameters
    View view;
    private String mParam1;
    private String mParam2;
    private OnFragmentInteractionListener mListener;
    private ListView doctorListView;
    private ChatDoctorAdapter adapter;
    private List<DoctorBean> doctorList = new ArrayList<DoctorBean>();

    public PatientConsult() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment PatientConsult.
     */
    // TODO: Rename and change types and number of parameters
    public static PatientConsult newInstance(String param1, String param2) {
        PatientConsult fragment = new PatientConsult();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public void setdoctorList(List<DoctorBean> doctors) {
        doctorList = doctors;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        if (view == null)
            view = inflater.inflate(R.layout.fragment_patient_consult, container, false);
        initlist(view);
        return view;
    }

    public void initlist(View view) {

        doctorListView = (ListView) view.findViewById(R.id.chat_doctor_list_view);
        adapter = new ChatDoctorAdapter(getActivity(), R.layout.item_chat_doctor, doctorList);
        doctorListView.setAdapter(adapter);
        doctorListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                DoctorBean doctor = doctorList.get(i);
                Data_whole.setDoctorBean(doctor);
                Intent intent = new Intent(getActivity(), PatientChat.class);
                startActivity(intent);
            }
        });
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


}

package com.linwankaifa.linwancunyi.ui.patient;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.linwankaifa.linwancunyi.R;
import com.linwankaifa.linwancunyi.bean.DoctorBean;
import com.linwankaifa.linwancunyi.ui.adapter.DoctorAdapter;
import com.linwankaifa.linwancunyi.util.Data_whole;
import com.linwankaifa.linwancunyi.util.JsonTransformation;

import java.io.IOException;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static android.widget.Toast.makeText;

public class PatientChooseDoctor extends AppCompatActivity {

    private ListView doctorListView;
    private DoctorAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patient_choosedoctor);

        initList();

    }

    public void choose(View view) {
        Intent intent = new Intent(PatientChooseDoctor.this, PatientChat.class);
        startActivity(intent);
    }

    public void back(View view) {
        this.finish();
    }

    private void initList() {
        OkHttpClient client = new OkHttpClient();
        RequestBody requestBody = null;

        String url = getString(R.string.url)+"/";
        url += "patient/getdoctorinfolist";

        Request request = new Request.Builder()
                .url(url)
                .build();
        okhttp3.Call call = client.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                // 如果失败，就显示失败信息
                Toast toast = makeText(getApplicationContext(), "请求失败", Toast.LENGTH_SHORT);
                toast.show();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                // 判断请求是否成功
                final String isSuccess = response.body().string();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (isSuccess.equals("404"))
                            makeText(getApplicationContext(), "错误请求404", Toast.LENGTH_SHORT).show();
                        else {
                            final List<DoctorBean> doctors = JsonTransformation.jsonToList(isSuccess, DoctorBean.class);
                            adapter = new DoctorAdapter(PatientChooseDoctor.this, R.layout.item_doctor, doctors);
                            doctorListView = (ListView) findViewById(R.id.choose_doctor_list_view);
                            doctorListView.setAdapter(adapter);

                            doctorListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                @Override
                                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                                    DoctorBean doctor = doctors.get(i);
                                    Data_whole.setDoctorBean(doctor);
                                    Intent intent = new Intent(PatientChooseDoctor.this, PatientChat.class);
                                    intent.putExtra("issend", "yes");
                                    startActivity(intent);
                                }
                            });
                        }
                    }
                });
            }
        });
    }
}

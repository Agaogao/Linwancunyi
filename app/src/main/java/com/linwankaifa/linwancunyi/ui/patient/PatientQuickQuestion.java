package com.linwankaifa.linwancunyi.ui.patient;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.linwankaifa.linwancunyi.R;

public class PatientQuickQuestion extends AppCompatActivity {

    TextView quick_question;
    String SHAREDPREFERENCES_NAME = "shared.xml";
    SharedPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patient_quickquestion);

        preferences = getSharedPreferences(SHAREDPREFERENCES_NAME, Context.MODE_PRIVATE);
    }

    public void doctor_choose(View view) {
        quick_question = (TextView) findViewById(R.id.editText);
        String question = quick_question.getText().toString();
        if(TextUtils.isEmpty(question)){
            Toast.makeText(this, "问题不能为空", Toast.LENGTH_SHORT).show();
            return;
        }
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("question_content", question);
        editor.commit();

        Intent intent = new Intent(PatientQuickQuestion.this, PatientChooseDoctor.class);
        startActivity(intent);
    }

    public void back(View view) {
        this.finish();
    }
}

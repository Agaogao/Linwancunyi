package com.linwankaifa.linwancunyi.ui.doctor;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.linwankaifa.linwancunyi.R;
import com.linwankaifa.linwancunyi.bean.DoctorBean;
import com.linwankaifa.linwancunyi.bean.HospitalBean;
import com.linwankaifa.linwancunyi.ui.patient.PatientInformationChange;
import com.linwankaifa.linwancunyi.util.Data_whole;
import com.linwankaifa.linwancunyi.util.JsonTransformation;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class DoctorOwnInformation extends AppCompatActivity {
    final HospitalBean hospital = new HospitalBean();
    TextView doctorname;
    TextView doctorage;
    TextView doctorsex;
    TextView doctordepartment;
    TextView doctorposition;
    TextView doctorspecilly;
    TextView doctorhospital;
    TextView doctoraccountnumber;
    String SHAREDPREFERENCES_NAME = "shared.xml";
    SharedPreferences preferences;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doctor_information);
        preferences = getSharedPreferences(SHAREDPREFERENCES_NAME, Context.MODE_PRIVATE);
        DoctorBean doctor = Data_whole.getDoctorBean();
        if (doctor.getDoctorname() == null) {
            doctor.setDoctorname("新用户");
        }
        if (doctor.getDoctorsex() == null) {
            doctor.setDoctorsex("");
        }
        if (doctor.getDoctorage() == null) {
            doctor.setDoctorage(20);
        }
        if (doctor.getDepartment() == null) {
            doctor.setDepartment("");
        }
        if (doctor.getPosition() == null) {
            doctor.setPosition("");
        }
        if (doctor.getSpeciality() == null) {
            doctor.setSpeciality("");
        }
        if (doctor.getHospitalid() == null) {
            doctor.setHospitalid(1101);
        }
        doctorname = (TextView) findViewById(R.id.doctor_name);
        doctorage = (TextView) findViewById(R.id.doctor_age);
        doctorsex = (TextView) findViewById(R.id.doctor_sex);
        doctordepartment = (TextView) findViewById(R.id.doctor_department);
        doctorposition = (TextView) findViewById(R.id.doctor_position);
        doctorspecilly = (TextView) findViewById(R.id.doctor_speciality);
        doctoraccountnumber = (TextView) findViewById(R.id.doctor_accountnumber);
        doctorname.setText(doctor.getDoctorname());
        doctorage.setText(String.valueOf(doctor.getDoctorage()));
        doctorsex.setText(doctor.getDoctorsex());
        doctordepartment.setText(doctor.getDepartment());
        doctorposition.setText(doctor.getPosition());
        doctorspecilly.setText(doctor.getSpeciality());
        doctoraccountnumber.setText(doctor.getDoctoraccountnumber());
        OkHttpClient client = new OkHttpClient();
        hospital.setHospitalid(doctor.getHospitalid());
        RequestBody requestBody = RequestBody.create(MediaType.parse("text; charset=utf-8"), JsonTransformation.objectToJson(hospital));
        String url = getString(R.string.url)+"/patient/gethospitalinfo";
        Request request = new Request.Builder()
                .url(url)
                .post(requestBody)
                .build();
        okhttp3.Call call = client.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Toast toast = Toast.makeText(getApplicationContext(), "医院信息获取失败", Toast.LENGTH_SHORT);
                toast.show();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                // 判断是否成功
                final String isSuccess = response.body().string();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (isSuccess.equals("404")) {
                            hospital.setHospitalname("");
                            doctorhospital = (TextView) findViewById(R.id.doctor_hospital);
                            doctorhospital.setText(hospital.getHospitalname());
                        } else {
                            final HospitalBean hospital = JsonTransformation.jsonToObject(isSuccess, HospitalBean.class);
                            doctorhospital = (TextView) findViewById(R.id.doctor_hospital);
                            doctorhospital.setText(hospital.getHospitalname());
                        }
                    }
                });
            }
        });
    }

    public void return_doctor_function(View view) {
        this.finish();
    }

    public void doctor_change_name(View view) {
        Intent intent = new Intent(DoctorOwnInformation.this, PatientInformationChange.class);
        intent.putExtra("title", "修改医生姓名");
        startActivity(intent);
    }

    public void doctor_change_department(View view) {
        Intent intent = new Intent(DoctorOwnInformation.this, PatientInformationChange.class);
        intent.putExtra("title", "修改医生科室");
        startActivity(intent);
    }

    public void doctor_change_hospital(View view) {
        Intent intent = new Intent(DoctorOwnInformation.this, PatientInformationChange.class);
        intent.putExtra("title", "修改医生所属医院");
        intent.putExtra("hospital", doctorhospital.getText());
        startActivity(intent);
    }

    public void doctor_change_post(View view) {
        Intent intent = new Intent(DoctorOwnInformation.this, PatientInformationChange.class);
        intent.putExtra("title", "修改医生职位");
        startActivity(intent);
    }

    public void doctor_change_speciality(View view) {
        Intent intent = new Intent(DoctorOwnInformation.this, PatientInformationChange.class);
        intent.putExtra("title", "修改医生擅长");
        startActivity(intent);
    }

    public void doctor_change_age(View view) {
        Intent intent = new Intent(DoctorOwnInformation.this, PatientInformationChange.class);
        intent.putExtra("title", "修改医生年龄");
        startActivity(intent);
    }

    public void doctor_change_gender(View view) {
        Intent intent = new Intent(DoctorOwnInformation.this, PatientInformationChange.class);
        intent.putExtra("title", "修改医生性别");
        startActivity(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        DoctorBean doctor = Data_whole.getDoctorBean();
        doctorname.setText(doctor.getDoctorname());
        doctorage.setText(String.valueOf(doctor.getDoctorage()));
        doctorsex.setText(doctor.getDoctorsex());
        doctordepartment.setText(doctor.getDepartment());
        doctorposition.setText(doctor.getPosition());
        doctorspecilly.setText(doctor.getSpeciality());
        doctoraccountnumber.setText(doctor.getDoctoraccountnumber());
        OkHttpClient client = new OkHttpClient();
        hospital.setHospitalid(doctor.getHospitalid());
        RequestBody requestBody = RequestBody.create(MediaType.parse("text; charset=utf-8"), JsonTransformation.objectToJson(hospital));
        String url = getString(R.string.url)+"/patient/gethospitalinfo";
        Request request = new Request.Builder()
                .url(url)
                .post(requestBody)
                .build();
        okhttp3.Call call = client.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Toast toast = Toast.makeText(getApplicationContext(), "医院信息获取失败", Toast.LENGTH_SHORT);
                toast.show();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                // 判断是否成功
                final String isSuccess = response.body().string();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (isSuccess.equals("404")) {
                            hospital.setHospitalname("");
                            doctorhospital = (TextView) findViewById(R.id.doctor_hospital);
                            doctorhospital.setText(hospital.getHospitalname());
                        } else if (isSuccess.equals("null")) {
                            hospital.setHospitalname("");
                            doctorhospital = (TextView) findViewById(R.id.doctor_hospital);
                            doctorhospital.setText(hospital.getHospitalname());
                        } else {
                            HospitalBean hospital = JsonTransformation.jsonToObject(isSuccess, HospitalBean.class);
                            doctorhospital = (TextView) findViewById(R.id.doctor_hospital);
                            doctorhospital.setText(hospital.getHospitalname());
                        }
                    }
                });
            }
        });
    }

    public void back(View view) {
        this.finish();
    }
}

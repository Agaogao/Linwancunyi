package com.linwankaifa.linwancunyi.ui.patient;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.linwankaifa.linwancunyi.R;
import com.linwankaifa.linwancunyi.bean.HospitalBean;
import com.linwankaifa.linwancunyi.util.Data_whole;
import com.linwankaifa.linwancunyi.util.IdiUtils;
import com.linwankaifa.linwancunyi.util.JsonTransformation;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class PatientFindHospital extends AppCompatActivity {
    EditText editText;
    private Map<Integer, Integer> hospitalIdMap = new HashMap<>();
    private List<HospitalBean> hospitalBeanList;
    private EditText search_content;
    private List<HospitalBean> fliterlist;
    private Map<Integer, Integer> flitermap;
    View.OnKeyListener onKey = new View.OnKeyListener() {
        @Override
        public boolean onKey(View v, int keyCode, KeyEvent event) {
            // TODO Auto-generated method stub
            if (event.getAction() == KeyEvent.ACTION_DOWN) {
                if (keyCode == KeyEvent.KEYCODE_DPAD_CENTER
                        || keyCode == KeyEvent.KEYCODE_ENTER) {

                    editText = (EditText) findViewById(R.id.hospitalinput);
                    String searchstr = editText.getText().toString();
                    // 界面适配器
                    LayoutInflater inflater = getLayoutInflater();
                    // 将要做容器的线性布局
                    LinearLayout rly = (LinearLayout) findViewById(R.id.hospital_list);
                    rly.removeAllViews();
                    TextView textView;
                    TextView textview1;
                    TextView textview2;
                    int i = 0;
                    fliterlist.clear();
                    for (HospitalBean hospitalBean : hospitalBeanList) {
                        if (hospitalBean.getHospitalname().contains(searchstr)) {
                            v = inflater.inflate(R.layout.list_hospital, null, false);
                            rly.addView(v);
                            fliterlist.add(hospitalBean);

                            // 每次获取一个textview 修改之后，改变id
                            textView = (TextView) findViewById(R.id.ggb_hospital_name);
                            textView.setText(hospitalBean.getHospitalname());
                            textView.setId(IdiUtils.generateViewId());

                            textview1 = (TextView) findViewById(R.id.ggb_hospital_city);
                            textview1.setText(hospitalBean.getArea());
                            textview1.setId(IdiUtils.generateViewId());

                            textview2 = (TextView) findViewById(R.id.ggb_hospital_content);
                            textview2.setText(hospitalBean.getIntroduction());
                            textview2.setId(IdiUtils.generateViewId());

                            int id = IdiUtils.generateViewId();
                            v.setId(id);
                            flitermap.put(id, i);
                            i++;
                        }
                    }

                    return true;
                }
            }

            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patient_findhospital);

        search_content = (EditText) findViewById(R.id.hospitalinput);
        search_content.setOnKeyListener(onKey);

        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(getString(R.string.url)+"/patient/gethospitalinfolist")
                .build();

        okhttp3.Call call = client.newCall(request);
        call.enqueue(new Callback() {

            @Override
            public void onFailure(Call call, IOException e) {
                Toast.makeText(getApplicationContext(), "登录失败", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                final String isSuccess = response.body().string();

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        switch (isSuccess) {
                            case "404":
                                Toast.makeText(getApplicationContext(), "查询医院错误", Toast.LENGTH_SHORT).show();
                                break;
                            case "":
                                // 如果成功过则进行业务逻辑操作
                                Toast.makeText(getApplicationContext(), "没有医院", Toast.LENGTH_SHORT).show();
                                break;
                            default:
                                // 将接收到的列表保存以便待会选取了对象之后来传输数据
                                hospitalBeanList = JsonTransformation.jsonToList(isSuccess, HospitalBean.class);
                                fliterlist = new ArrayList<HospitalBean>();
                                fliterlist.addAll(hospitalBeanList);
                                // 将要修改的控件先声明
                                TextView textView;
                                TextView textview1;
                                TextView textview2;
                                HospitalBean hospitalBean;

                                // 将要插入进去的布局
                                View view;

                                // 界面适配器
                                LayoutInflater inflater = getLayoutInflater();
                                // 将要做容器的线性布局
                                LinearLayout rly = (LinearLayout) findViewById(R.id.hospital_list);

                                // 遍历一遍
                                Iterator<HospitalBean> iterator = hospitalBeanList.iterator();


                                // 初始化idmap

                                int i = 0;
                                while (iterator.hasNext()) {
                                    hospitalBean = iterator.next();

                                    // 每次新创一个布局的实例，加入到线性布局中
                                    view = inflater.inflate(R.layout.list_hospital, null, false);
                                    rly.addView(view);

                                    // 每次获取一个textview 修改之后，改变id
                                    textView = (TextView) findViewById(R.id.ggb_hospital_name);
                                    textView.setText(hospitalBean.getHospitalname());
                                    textView.setId(IdiUtils.generateViewId());

                                    textview1 = (TextView) findViewById(R.id.ggb_hospital_city);
                                    textview1.setText(hospitalBean.getArea());
                                    textview1.setId(IdiUtils.generateViewId());

                                    textview2 = (TextView) findViewById(R.id.ggb_hospital_content);
                                    textview2.setText(hospitalBean.getIntroduction());
                                    textview2.setId(IdiUtils.generateViewId());

                                    int id = IdiUtils.generateViewId();
                                    view.setId(id);
                                    hospitalIdMap.put(id, i);
                                    i++;
                                }
                                flitermap = hospitalIdMap;
                                break;
                        }
                    }
                });
            }
        });

    }

    public void hospital_choose(View view) {

    }

    public void clickhospital(View view) {
        // 将所点击的
        Intent intent = new Intent(PatientFindHospital.this, PatientFindHospitalResult.class);
        Data_whole.setHospitalBean(fliterlist.get(flitermap.get(view.getId())));
        startActivity(intent);
    }

    public void back(View view) {
        this.finish();
    }


}

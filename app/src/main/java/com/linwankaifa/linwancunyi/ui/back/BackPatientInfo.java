package com.linwankaifa.linwancunyi.ui.back;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.linwankaifa.linwancunyi.R;
import com.linwankaifa.linwancunyi.bean.PatientBean;
import com.linwankaifa.linwancunyi.util.Data_whole;

public class BackPatientInfo extends AppCompatActivity {
    TextView patient_name;
    TextView patient_petname;
    TextView patient_accountnumber;
    TextView patient_sex;
    TextView patient_age;
    TextView patient_phone;
    TextView patient_birthday;
    String SHAREDPREFERENCES_NAME = "shared.xml";
    SharedPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patient_info);
        patient_name = (TextView) findViewById(R.id.patient_name);
        patient_petname = (TextView) findViewById(R.id.patient_petname);
        patient_accountnumber = (TextView) findViewById(R.id.patient_accountnumber);
        patient_sex = (TextView) findViewById(R.id.patient_sex);
        patient_age = (TextView) findViewById(R.id.patient_age);
        patient_phone = (TextView) findViewById(R.id.patient_phone);
        patient_birthday = (TextView) findViewById(R.id.patient_birthday);
        PatientBean patient = Data_whole.getPatientBean();
        patient_name.setText(patient.getPatientname());
        patient_petname.setText(patient.getPetname());
        patient_accountnumber.setText(patient.getAccountnumber());
        patient_sex.setText(patient.getPatientsex());
        patient_age.setText(String.valueOf(patient.getPatientage()));
        patient_phone.setText(patient.getPhone());
        patient_birthday.setText(patient.getBirthday());

    }

    public void back(View view) {
        this.finish();
    }
}

package com.linwankaifa.linwancunyi.ui.patient;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.linwankaifa.linwancunyi.R;
import com.linwankaifa.linwancunyi.bean.HuatiBean;
import com.linwankaifa.linwancunyi.util.Data_whole;

public class PatientArticle_content extends AppCompatActivity {
    TextView articlebrief;
    TextView articlecontent;
    String SHAREDPREFERENCES_NAME = "shared.xml";
    SharedPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patien_article_content);
        preferences = getSharedPreferences(SHAREDPREFERENCES_NAME, Context.MODE_PRIVATE);
        articlebrief = (TextView) findViewById(R.id.articlebrief);
        articlecontent = (TextView) findViewById(R.id.articlecontent);
        HuatiBean huati = Data_whole.getHuatiBean();
        articlebrief.setText(huati.getBriefintroduction());
        articlecontent.setText(huati.getFabucontent());
    }

    public void back(View view) {
        this.finish();
    }
}

package com.linwankaifa.linwancunyi.ui.patient;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.linwankaifa.linwancunyi.R;
import com.linwankaifa.linwancunyi.bean.HospitalBean;
import com.linwankaifa.linwancunyi.util.Data_whole;

public class PatientFindHospitalResult extends AppCompatActivity {
    TextView textView1;
    TextView textView2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        // 获取医院列表
        // 获取上一个界面传过来的点击的第几个页面
        HospitalBean hospitalBean = Data_whole.getHospitalBean();

        setContentView(R.layout.activity_patient_findhospitalresult);
        textView1 = (TextView) findViewById(R.id.findhospitalresult_textview1);
        textView2 = (TextView) findViewById(R.id.findhospitalresult_textview2);
        textView1.setText(hospitalBean.getHospitalname());
        textView2.setText(hospitalBean.getIntroduction());

    }

    public void back(View view) {
        this.finish();
    }
}

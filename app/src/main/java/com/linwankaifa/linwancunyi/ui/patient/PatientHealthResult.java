package com.linwankaifa.linwancunyi.ui.patient;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.widget.TextView;

import com.linwankaifa.linwancunyi.R;

public class PatientHealthResult extends AppCompatActivity {

    TextView health_result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patient_healthresult);

        health_result = (TextView) findViewById(R.id.health_result);
        Intent previous = getIntent();
        health_result.setText(previous.getStringExtra("result"));
    }

    public void back(View view) {
        Intent intent = new Intent(PatientHealthResult.this, PatientMain.class);
        startActivity(intent);
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {

        Intent intent = new Intent(PatientHealthResult.this, PatientMain.class);
        startActivity(intent);

        return super.onKeyDown(keyCode, event);

    }
}

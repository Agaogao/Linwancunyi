package com.linwankaifa.linwancunyi.ui.patient;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.linwankaifa.linwancunyi.R;
import com.linwankaifa.linwancunyi.bean.DoctorBean;
import com.linwankaifa.linwancunyi.bean.HospitalBean;
import com.linwankaifa.linwancunyi.bean.PatientBean;
import com.linwankaifa.linwancunyi.util.Data_whole;
import com.linwankaifa.linwancunyi.util.JsonTransformation;

import java.io.IOException;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by 余梦麒 on 2019/7/15.
 */

public class PatientInformationChange extends AppCompatActivity {
    TextView changeinformation_title;
    EditText changeingormation_content;
    String title;
    DoctorBean doctorBean = Data_whole.getDoctorBean();
    PatientBean patientBean = Data_whole.getPatientBean();
    String SHAREDPREFERENCES_NAME = "shared.xml";
    SharedPreferences preferences;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_information_change);
        Intent intent = getIntent();
        title = intent.getStringExtra("title");
        String hospitalname = intent.getStringExtra("hospital");
        changeinformation_title = (TextView) findViewById(R.id.changeinformation_title);
        changeingormation_content = (EditText) findViewById(R.id.changeinformation_content);
        switch (title) {
            case "修改医生姓名":
                changeinformation_title.setText("修改姓名");
                changeingormation_content.setText(doctorBean.getDoctorname());
                break;
            case "修改医生年龄":
                changeinformation_title.setText("修改年龄");
                changeingormation_content.setText(String.valueOf(doctorBean.getDoctorage()));
                break;
            case "修改医生性别":
                changeinformation_title.setText("修改性别");
                changeingormation_content.setText(doctorBean.getDoctorsex());
                break;
            case "修改医生科室":
                changeinformation_title.setText("修改科室");
                changeingormation_content.setText(doctorBean.getDepartment());
                break;
            case "修改医生职位":
                changeinformation_title.setText("修改职位");
                changeingormation_content.setText(doctorBean.getPosition());
                break;
            case "修改医生所属医院":
                changeinformation_title.setText("修改所属医院");
                changeingormation_content.setText(hospitalname);
                break;
            case "修改医生擅长":
                changeinformation_title.setText("修改擅长");
                changeingormation_content.setText(doctorBean.getSpeciality());
                break;
            case "修改患者姓名":
                changeinformation_title.setText("修改姓名");
                changeingormation_content.setText(patientBean.getPatientname());
                break;
            case "修改患者昵称":
                changeinformation_title.setText("修改昵称");
                changeingormation_content.setText(patientBean.getPetname());
                break;
            case "修改患者年龄":
                changeinformation_title.setText("修改年龄");
                changeingormation_content.setText(String.valueOf(patientBean.getPatientage()));
                break;
            case "修改患者性别":
                changeinformation_title.setText("修改性别");
                changeingormation_content.setText(patientBean.getPatientsex());
                break;
            case "修改患者电话":
                changeinformation_title.setText("修改电话");
                changeingormation_content.setText(patientBean.getPhone());
                break;
            case "修改患者生日":
                changeinformation_title.setText("修改生日");
                changeingormation_content.setText(patientBean.getBirthday());
                break;
            default:
                break;
        }

    }

    public void changefinish(View view) {
        if (changeingormation_content.getText().toString().equals("")) {
            Toast toast = Toast.makeText(getApplicationContext(), "输入不能为空", Toast.LENGTH_SHORT);
            toast.show();
        } else {
            preferences = getSharedPreferences(SHAREDPREFERENCES_NAME, Context.MODE_PRIVATE);
            String usertype = preferences.getString("usertype", "");
            if (usertype.equals("patient")) {
                changePatineInformation();
            } else {
                changeDoctorInformation();
            }
        }
    }

    public void changePatineInformation() {
        switch (title) {
            case "修改患者姓名":
                patientBean.setPatientname(changeingormation_content.getText().toString());
                break;
            case "修改患者昵称":
                patientBean.setPetname(changeingormation_content.getText().toString());
                break;
            case "修改患者年龄":
                patientBean.setPatientage(Integer.valueOf(changeingormation_content.getText().toString()));
                break;
            case "修改患者性别":
                patientBean.setPatientsex(changeingormation_content.getText().toString());
                break;
            case "修改患者电话":
                patientBean.setPhone(changeingormation_content.getText().toString());
                break;
            case "修改患者生日":
                patientBean.setBirthday(changeingormation_content.getText().toString());
                break;
            default:
                break;
        }
        OkHttpClient client = new OkHttpClient();
        RequestBody requestBody = RequestBody.create(MediaType.parse("text; charset=utf-8"), JsonTransformation.objectToJson(patientBean));
        String url = getString(R.string.url)+"/patient/updatepatientinfo";
        Request request = new Request.Builder()
                .url(url)
                .post(requestBody)
                .build();
        okhttp3.Call call = client.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Toast toast = Toast.makeText(getApplicationContext(), "患者信息获取失败", Toast.LENGTH_SHORT);
                toast.show();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                // 判断是否成功
                final String isSuccess = response.body().string();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (isSuccess.equals("404")) {
                            Toast toast = Toast.makeText(getApplicationContext(), "修改失败", Toast.LENGTH_SHORT);
                            toast.show();
                        } else {
                            Toast toast = Toast.makeText(getApplicationContext(), "修改成功", Toast.LENGTH_SHORT);
                            Data_whole.setPatientBean(patientBean);
                            Log.e("dd", patientBean.toString());
                            toast.show();
                        }
                    }
                });
            }
        });
        this.finish();
    }

    public void changeDoctorInformation() {
        int flag;
        switch (title) {
            case "修改医生姓名":
                doctorBean.setDoctorname(changeingormation_content.getText().toString());
                flag = 1;
                break;
            case "修改医生年龄":
                doctorBean.setDoctorage(Integer.valueOf(changeingormation_content.getText().toString()));
                flag = 1;
                break;
            case "修改医生性别":
                doctorBean.setDoctorsex(changeingormation_content.getText().toString());
                flag = 1;
                break;
            case "修改医生科室":
                doctorBean.setDepartment(changeingormation_content.getText().toString());
                flag = 1;
                break;
            case "修改医生职位":
                doctorBean.setPosition(changeingormation_content.getText().toString());
                flag = 1;
                break;
            case "修改医生擅长":
                doctorBean.setSpeciality(changeingormation_content.getText().toString());
                flag = 1;
                break;
            default:
                flag = 0;
                break;
        }
        if (flag == 1) {
            OkHttpClient client = new OkHttpClient();
            RequestBody requestBody = RequestBody.create(MediaType.parse("text; charset=utf-8"), JsonTransformation.objectToJson(doctorBean));
            String url = getString(R.string.url)+"/doctor/updatedoctorinfo";
            Request request = new Request.Builder()
                    .url(url)
                    .post(requestBody)
                    .build();
            okhttp3.Call call = client.newCall(request);
            call.enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    Toast toast = Toast.makeText(getApplicationContext(), "医生信息获取失败", Toast.LENGTH_SHORT);
                    toast.show();
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    // 判断是否成功
                    final String isSuccess = response.body().string();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (isSuccess.equals("404")) {
                                Toast toast = Toast.makeText(getApplicationContext(), "修改失败", Toast.LENGTH_SHORT);
                                toast.show();
                            } else {
                                Toast toast = Toast.makeText(getApplicationContext(), "修改成功", Toast.LENGTH_SHORT);
                                Data_whole.setDoctorBean(doctorBean);
                                Log.e("dd", doctorBean.toString());
                                toast.show();
                            }
                        }
                    });
                }
            });
            this.finish();
        } else {
            if (title.equals("修改医生所属医院")) {
                OkHttpClient client = new OkHttpClient();
                String url = getString(R.string.url)+"/patient/gethospitalinfolist";
                Request request = new Request.Builder()
                        .url(url)
                        .get()
                        .build();
                okhttp3.Call call = client.newCall(request);
                call.enqueue(new Callback() {
                    @Override
                    public void onFailure(Call call, IOException e) {
                        Toast toast = Toast.makeText(getApplicationContext(), "医院信息获取失败", Toast.LENGTH_SHORT);
                        toast.show();
                    }

                    @Override
                    public void onResponse(Call call, Response response) throws IOException {
                        // 判断是否成功
                        final String isSuccess = response.body().string();
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (isSuccess.equals("404")) {
                                    Toast toast = Toast.makeText(getApplicationContext(), "获取医院列表失败", Toast.LENGTH_SHORT);
                                    toast.show();
                                }  else {
                                    List<HospitalBean> hospitallist = JsonTransformation.jsonToList(isSuccess, HospitalBean.class);
                                    Log.e("ds", String.valueOf(hospitallist.size()));
                                    int i;
                                    for(i = 0;i < hospitallist.size();i++)
                                    {
                                        if(changeingormation_content.getText().toString().equals(hospitallist.get(i).getHospitalname()))
                                        {
                                            doctorBean.setHospitalid(hospitallist.get(i).getHospitalid());
                                            break;
                                        }
                                    }
                                    if(i == hospitallist.size())
                                    {
                                       Toast.makeText(getApplicationContext(), "该医院不存在", Toast.LENGTH_SHORT).show();
                                    }
                                    else {
                                        OkHttpClient client = new OkHttpClient();
                                        RequestBody requestBody = RequestBody.create(MediaType.parse("text; charset=utf-8"), JsonTransformation.objectToJson(doctorBean));
                                        String url = getString(R.string.url)+"/doctor/updatedoctorinfo";
                                        Request request = new Request.Builder()
                                                .url(url)
                                                .post(requestBody)
                                                .build();
                                        okhttp3.Call call = client.newCall(request);
                                        call.enqueue(new Callback() {
                                            @Override
                                            public void onFailure(Call call, IOException e) {
                                                Toast toast = Toast.makeText(getApplicationContext(), "医生信息获取失败", Toast.LENGTH_SHORT);
                                                toast.show();
                                            }

                                            @Override
                                            public void onResponse(Call call, Response response) throws IOException {
                                                // 判断是否成功
                                                final String isSuccess = response.body().string();
                                                runOnUiThread(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        if (isSuccess.equals("404")) {
                                                            Toast toast = Toast.makeText(getApplicationContext(), "修改失败", Toast.LENGTH_SHORT);
                                                            toast.show();
                                                        } else {
                                                            Toast toast = Toast.makeText(getApplicationContext(), "修改成功", Toast.LENGTH_SHORT);
                                                            Data_whole.setDoctorBean(doctorBean);
                                                            Log.e("dd", doctorBean.toString());
                                                            toast.show();
                                                            PatientInformationChange.this.finish();
                                                        }
                                                    }
                                                });
                                            }
                                        });
                                    }
                                }
                            }
                        });
                    }
                });
            }
        }
    }

    public void back(View view) {
        this.finish();
    }
}

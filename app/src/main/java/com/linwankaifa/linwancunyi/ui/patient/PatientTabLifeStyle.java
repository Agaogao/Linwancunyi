package com.linwankaifa.linwancunyi.ui.patient;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.linwankaifa.linwancunyi.R;

public class PatientTabLifeStyle extends AppCompatActivity {

    TextView tv_couyan;
    TextView tv_yingjiu;
    TextView tv_yingshi;
    TextView tv_shuimian;
    TextView tv_daxiaobian;
    TextView tv_chuimianyao;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tab_life_style);

        tv_couyan = (TextView) findViewById(R.id.tv_couyan);
        tv_yingjiu = (TextView) findViewById(R.id.tv_yingjiu);
        tv_yingshi = (TextView) findViewById(R.id.tv_yingshi);
        tv_shuimian = (TextView) findViewById(R.id.tv_shuimian);
        tv_daxiaobian = (TextView) findViewById(R.id.tv_daxiaobian);
        tv_chuimianyao = (TextView) findViewById(R.id.tv_chuimianyao);
    }

    protected void dialog() {
        Dialog dialog = new AlertDialog.Builder(this).setIcon(
                android.R.drawable.btn_star).setTitle("是否抽烟").setMessage(
                "请选择？").setPositiveButton("是",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub
                        tv_couyan.setText("是");
                    }
                }).setNegativeButton("否", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // TODO Auto-generated method stub
                tv_couyan.setText("否");
            }
        }).setNeutralButton("已戒烟", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // TODO Auto-generated method stub
                tv_couyan.setText("已戒烟");
            }
        }).setNeutralButton("取消", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // TODO Auto-generated method stub

            }
        }).create();
        dialog.show();
    }

    protected void dialog1() {
        Dialog dialog = new AlertDialog.Builder(this).setIcon(
                android.R.drawable.btn_star).setTitle("是否饮酒").setMessage(
                "请选择？").setPositiveButton("从不",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub
                        tv_yingjiu.setText("从不");
                    }
                }).setNegativeButton("偶尔", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // TODO Auto-generated method stub
                tv_yingjiu.setText("偶尔");
            }
        }).setNeutralButton("经常", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // TODO Auto-generated method stub
                tv_yingjiu.setText("经常");
            }
        }).setNeutralButton("每天", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // TODO Auto-generated method stub
                tv_yingjiu.setText("每天");
            }
        }).setNeutralButton("取消", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // TODO Auto-generated method stub
            }
        }).create();
        dialog.show();
    }

    protected void dialog2() {
        Dialog dialog = new AlertDialog.Builder(this).setIcon(
                android.R.drawable.btn_star).setTitle("饮食是否规律").setMessage(
                "请选择？").setPositiveButton("是",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub
                        tv_yingshi.setText("是");
                    }
                }).setNegativeButton("否", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // TODO Auto-generated method stub
                tv_yingshi.setText("否");
            }
        }).setNeutralButton("取消", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // TODO Auto-generated method stub

            }
        }).create();
        dialog.show();
    }

    protected void dialog3() {
        Dialog dialog = new AlertDialog.Builder(this).setIcon(
                android.R.drawable.btn_star).setTitle("睡眠是否规律").setMessage(
                "请选择？").setPositiveButton("是",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub
                        tv_shuimian.setText("是");
                    }
                }).setNegativeButton("否", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // TODO Auto-generated method stub
                tv_shuimian.setText("否");
            }
        }).setNeutralButton("取消", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // TODO Auto-generated method stub

            }
        }).create();
        dialog.show();
    }

    protected void dialog4() {
        Dialog dialog = new AlertDialog.Builder(this).setIcon(
                android.R.drawable.btn_star).setTitle("大小便是否正常").setMessage(
                "请选择？").setPositiveButton("是",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub
                        tv_daxiaobian.setText("是");
                    }
                }).setNegativeButton("否", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // TODO Auto-generated method stub
                tv_daxiaobian.setText("否");
            }
        }).setNeutralButton("取消", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // TODO Auto-generated method stub

            }
        }).create();
        dialog.show();
    }

    protected void dialog5() {
        Dialog dialog = new AlertDialog.Builder(this).setIcon(
                android.R.drawable.btn_star).setTitle("是否长期服用止痛药或镇定催眠药").setMessage(
                "请选择？").setPositiveButton("是",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub
                        tv_chuimianyao.setText("是");
                    }
                }).setNegativeButton("否", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // TODO Auto-generated method stub
                tv_chuimianyao.setText("否");
            }
        }).setNeutralButton("取消", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // TODO Auto-generated method stub

            }
        }).create();
        dialog.show();
    }

    public void cyclick(View view) {
        dialog();
    }

    public void yjclick(View view) {
        dialog1();
    }

    public void ysclick(View view) {
        dialog2();
    }

    public void smclick(View view) {
        dialog3();
    }

    public void dxbclick(View view) {
        dialog4();
    }

    public void cmyclick(View view) {
        dialog5();
    }
}

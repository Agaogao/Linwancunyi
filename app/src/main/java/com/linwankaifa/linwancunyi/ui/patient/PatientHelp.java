package com.linwankaifa.linwancunyi.ui.patient;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.linwankaifa.linwancunyi.R;

/**
 * Created by 余梦麒 on 2019/7/15.
 */

public class PatientHelp extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help);
    }

    public void back(View view) {
        this.finish();
    }
}

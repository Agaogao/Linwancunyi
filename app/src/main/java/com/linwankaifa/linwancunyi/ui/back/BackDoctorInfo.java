package com.linwankaifa.linwancunyi.ui.back;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.linwankaifa.linwancunyi.R;
import com.linwankaifa.linwancunyi.util.Data_whole;

public class BackDoctorInfo extends AppCompatActivity {

    TextView tvName;
    TextView tvSex;
    TextView tvAge;
    TextView tvDepartment;
    TextView tvPosition;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doctor_info);

        tvName = (TextView) findViewById(R.id.back_doctor_name);
        tvAge = (TextView) findViewById(R.id.back_doctor_age);
        tvSex = (TextView) findViewById(R.id.back_doctor_sex);
        tvDepartment = (TextView) findViewById(R.id.back_doctor_department);
        tvPosition = (TextView) findViewById(R.id.back_doctor_position);

        tvName.setText(Data_whole.getDoctorBean().getDoctorname());
        tvAge.setText(String.valueOf(Data_whole.getDoctorBean().getDoctorage()));
        tvSex.setText(Data_whole.getDoctorBean().getDoctorsex());
        tvDepartment.setText(Data_whole.getDoctorBean().getDepartment());
        tvPosition.setText(Data_whole.getDoctorBean().getPosition());
    }

    public void back(View view) {
        this.finish();
    }
}

package com.linwankaifa.linwancunyi.ui.patient;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.linwankaifa.linwancunyi.R;
import com.linwankaifa.linwancunyi.bean.SymptomaticBean;
import com.linwankaifa.linwancunyi.util.Data_whole;

public class PatientSDOSResult extends AppCompatActivity {
    TextView symptomatictitle;
    TextView symptomaticresult;
    String SHAREDPREFERENCES_NAME = "shared.xml";
    SharedPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patient_sdosresult);
        preferences = getSharedPreferences(SHAREDPREFERENCES_NAME, Context.MODE_PRIVATE);
        symptomatictitle = (TextView) findViewById(R.id.symptomatic_title);
        symptomaticresult = (TextView) findViewById(R.id.symptomatic_result);
        SymptomaticBean symptomatic = Data_whole.getSymptomaticBean();
        symptomatictitle.setText(symptomatic.getSymptomatic());
        symptomaticresult.setText(symptomatic.getDetail());
    }

    public void back(View view) {
        this.finish();
    }
}

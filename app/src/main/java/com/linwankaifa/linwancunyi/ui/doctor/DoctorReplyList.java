package com.linwankaifa.linwancunyi.ui.doctor;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.linwankaifa.linwancunyi.R;
import com.linwankaifa.linwancunyi.bean.PatientBean;
import com.linwankaifa.linwancunyi.ui.adapter.ChatPatientAdapter;
import com.linwankaifa.linwancunyi.util.Data_whole;
import com.linwankaifa.linwancunyi.util.JsonTransformation;

import java.io.IOException;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static android.widget.Toast.makeText;

/**
 * Created by 余梦麒 on 2019/7/15.
 */

public class DoctorReplyList extends AppCompatActivity {

    List<PatientBean> patientList;
    private ListView patientListView;
    private ChatPatientAdapter adapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doctor_replylist);

        initList();
    }

    
    private void initList() {
        OkHttpClient client = new OkHttpClient();
        RequestBody requestBody = RequestBody.create(MediaType.parse("text; charset=utf-8"),
                JsonTransformation.objectToJson(Data_whole.getDoctorBean()));

        String url = getString(R.string.url)+"/";
        url += "doctor/getchatedpatients";

        Request request = new Request.Builder()
                .url(url)
                .post(requestBody)
                .build();
        okhttp3.Call call = client.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                // 如果失败，就显示失败信息
                Toast toast = makeText(getApplicationContext(), "请求失败", Toast.LENGTH_SHORT);
                toast.show();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                // 判断请求是否成功
                final String isSuccess = response.body().string();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (isSuccess.equals("404"))
                            makeText(getApplicationContext(), "错误请求404", Toast.LENGTH_SHORT).show();
                        else {
                            patientList = JsonTransformation.jsonToList(isSuccess, PatientBean.class);
                            try {
                                adapter = new ChatPatientAdapter(DoctorReplyList.this, R.layout.item_chat_patient, patientList);
                                patientListView = (ListView) findViewById(R.id.chat_patient_list_view);
                                patientListView.setAdapter(adapter);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            patientListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                @Override
                                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                                    PatientBean patient = patientList.get(i);
                                    Data_whole.setPatientBean(patient);
                                    Log.e("replylist", Data_whole.getPatientBean().toString());
                                    Intent intent = new Intent(DoctorReplyList.this, DoctorChat.class);
                                    startActivity(intent);
                                }
                            });
                        }
                    }
                });
            }
        });
    }
}

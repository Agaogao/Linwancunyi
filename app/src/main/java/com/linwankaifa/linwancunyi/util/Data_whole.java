package com.linwankaifa.linwancunyi.util;

import com.linwankaifa.linwancunyi.bean.ChatrecordBean;
import com.linwankaifa.linwancunyi.bean.DoctorBean;
import com.linwankaifa.linwancunyi.bean.HealthassessmentQuestionBean;
import com.linwankaifa.linwancunyi.bean.HealthassessmentResultBean;
import com.linwankaifa.linwancunyi.bean.HospitalBean;
import com.linwankaifa.linwancunyi.bean.HuatiBean;
import com.linwankaifa.linwancunyi.bean.ManagerBean;
import com.linwankaifa.linwancunyi.bean.PatientBean;
import com.linwankaifa.linwancunyi.bean.StaticDataBean;
import com.linwankaifa.linwancunyi.bean.SymptomaticBean;

/**
 * Created by 邹京汕 on 2019/7/14.
 */

public class Data_whole {
    private static ChatrecordBean chatRecordBean;
    private static DoctorBean doctorBean;
    private static HealthassessmentQuestionBean healthAssessment_questionBean;
    private static HealthassessmentResultBean healthAssessment_resultBean;
    private static HospitalBean hospitalBean;
    private static HuatiBean huatiBean;
    private static ManagerBean managerBean;
    private static PatientBean patientBean;
    private static StaticDataBean staticDataBean = new StaticDataBean();
    private static SymptomaticBean symptomaticBean;

    public static ChatrecordBean getChatRecordBean() {
        return chatRecordBean;
    }

    public static void setChatRecordBean(ChatrecordBean chatRecordBean) {
        Data_whole.chatRecordBean = chatRecordBean;
    }

    public static DoctorBean getDoctorBean() {
        return doctorBean;
    }

    public static void setDoctorBean(DoctorBean doctorBean) {
        Data_whole.doctorBean = doctorBean;
    }

    public static HealthassessmentQuestionBean getHealthAssessment_questionBean() {
        return healthAssessment_questionBean;
    }

    public static void setHealthAssessment_questionBean(HealthassessmentQuestionBean healthAssessment_questionBean) {
        Data_whole.healthAssessment_questionBean = healthAssessment_questionBean;
    }

    public static HealthassessmentResultBean getHealthAssessment_resultBean() {
        return healthAssessment_resultBean;
    }

    public static void setHealthAssessment_resultBean(HealthassessmentResultBean healthAssessment_resultBean) {
        Data_whole.healthAssessment_resultBean = healthAssessment_resultBean;
    }

    public static HospitalBean getHospitalBean() {
        return hospitalBean;
    }

    public static void setHospitalBean(HospitalBean hospitalBean) {
        Data_whole.hospitalBean = hospitalBean;
    }

    public static HuatiBean getHuatiBean() {
        return huatiBean;
    }

    public static void setHuatiBean(HuatiBean huatiBean) {
        Data_whole.huatiBean = huatiBean;
    }

    public static ManagerBean getManagerBean() {
        return managerBean;
    }

    public static void setManagerBean(ManagerBean managerBean) {
        Data_whole.managerBean = managerBean;
    }

    public static PatientBean getPatientBean() {
        return patientBean;
    }

    public static void setPatientBean(PatientBean patientBean) {
        Data_whole.patientBean = patientBean;
    }

    public static StaticDataBean getStaticDataBean() {
        return staticDataBean;
    }

    public static void setStaticDataBean(StaticDataBean staticDataBean) {
        Data_whole.staticDataBean = staticDataBean;
    }

    public static SymptomaticBean getSymptomaticBean() {
        return symptomaticBean;
    }

    public static void setSymptomaticBean(SymptomaticBean symptomaticBean) {
        Data_whole.symptomaticBean = symptomaticBean;
    }

}

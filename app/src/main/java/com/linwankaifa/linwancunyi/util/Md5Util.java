package com.linwankaifa.linwancunyi.util;

/**
 * Created by 邹京汕 on 2019/7/14.
 */

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Md5Util {

    static String[] chars =
            {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d", "e", "f"};

    public static String StringInMd5(String str) {

        MessageDigest md5 = null;
        try {
            md5 = MessageDigest.getInstance("md5");
            byte[] result = md5.digest(str.getBytes());

            StringBuilder sb = new StringBuilder(32);
            for (int i = 0; i < result.length; i++) {
                byte x = result[i];
                // 取得高位
                int h = 0x0f & (x >>> 4);
                // 取得低位
                int l = 0x0f & x;
                sb.append(chars[h]).append(chars[l]);
            }
            return sb.toString();

        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }

    public static void main(String[] args) {
        System.out.println(Md5Util.StringInMd5("123456"));
    }
}

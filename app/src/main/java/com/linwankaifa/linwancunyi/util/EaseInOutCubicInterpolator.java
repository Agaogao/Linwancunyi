package com.linwankaifa.linwancunyi.util;

import android.animation.TimeInterpolator;

/**
 * Created by 邹京汕 on 2019/7/18.
 */


public class EaseInOutCubicInterpolator implements TimeInterpolator {

    @Override

    public float getInterpolation(float input) {

        if ((input *= 2) < 1.0f) {

            return 0.5f * input * input * input;

        }

        input -= 2;

        return 0.5f * input * input * input + 1;

    }

}

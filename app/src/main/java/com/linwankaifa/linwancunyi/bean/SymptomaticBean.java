package com.linwankaifa.linwancunyi.bean;

import java.io.Serializable;

public class SymptomaticBean implements Serializable {
    private static final long serialVersionUID = 1L;
    private Integer symptomaticid;
    private String symptomaticsite;
    private String symptomatic;
    private String detail;

    public Integer getSymptomaticid() {
        return symptomaticid;
    }

    public void setSymptomaticid(Integer symptomaticid) {
        this.symptomaticid = symptomaticid;
    }

    public String getSymptomaticsite() {
        return symptomaticsite;
    }

    public void setSymptomaticsite(String symptomaticsite) {
        this.symptomaticsite = symptomaticsite;
    }

    public String getSymptomatic() {
        return symptomatic;
    }

    public void setSymptomatic(String symptomatic) {
        this.symptomatic = symptomatic;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", symptomaticid=").append(symptomaticid);
        sb.append(", symptomaticsite=").append(symptomaticsite);
        sb.append(", symptomatic=").append(symptomatic);
        sb.append(", detail=").append(detail);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}
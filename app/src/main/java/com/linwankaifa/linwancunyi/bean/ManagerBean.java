package com.linwankaifa.linwancunyi.bean;

import java.io.Serializable;

public class ManagerBean implements Serializable {
    private static final long serialVersionUID = 1L;
    private Integer managerid;
    private String manageraccountnumber;
    private String managerpassword;

    public Integer getManagerid() {
        return managerid;
    }

    public void setManagerid(Integer managerid) {
        this.managerid = managerid;
    }

    public String getManageraccountnumber() {
        return manageraccountnumber;
    }

    public void setManageraccountnumber(String manageraccountnumber) {
        this.manageraccountnumber = manageraccountnumber;
    }

    public String getManagerpassword() {
        return managerpassword;
    }

    public void setManagerpassword(String managerpassword) {
        this.managerpassword = managerpassword;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", managerid=").append(managerid);
        sb.append(", manageraccountnumber=").append(manageraccountnumber);
        sb.append(", managerpassword=").append(managerpassword);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}
package com.linwankaifa.linwancunyi.bean;

import java.io.Serializable;

public class HealthassessmentResultBean implements Serializable {
    private static final long serialVersionUID = 1L;
    private Integer resultid;
    private Integer grade;
    private String resultcontent;

    public Integer getResultid() {
        return resultid;
    }

    public void setResultid(Integer resultid) {
        this.resultid = resultid;
    }

    public Integer getGrade() {
        return grade;
    }

    public void setGrade(Integer grade) {
        this.grade = grade;
    }

    public String getResultcontent() {
        return resultcontent;
    }

    public void setResultcontent(String resultcontent) {
        this.resultcontent = resultcontent;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", resultid=").append(resultid);
        sb.append(", grade=").append(grade);
        sb.append(", resultcontent=").append(resultcontent);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}
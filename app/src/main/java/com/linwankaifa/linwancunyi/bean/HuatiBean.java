package com.linwankaifa.linwancunyi.bean;

import java.io.Serializable;

public class HuatiBean implements Serializable {
    private static final long serialVersionUID = 1L;
    private Integer huatiid;
    private Integer doctorid;
    private String fabudate;
    private String briefintroduction;
    private String fabucontent;

    public Integer getHuatiid() {
        return huatiid;
    }

    public void setHuatiid(Integer huatiid) {
        this.huatiid = huatiid;
    }

    public Integer getDoctorid() {
        return doctorid;
    }

    public void setDoctorid(Integer doctorid) {
        this.doctorid = doctorid;
    }

    public String getFabudate() {
        return fabudate;
    }

    public void setFabudate(String fabudate) {
        this.fabudate = fabudate;
    }

    public String getBriefintroduction() {
        return briefintroduction;
    }

    public void setBriefintroduction(String briefintroduction) {
        this.briefintroduction = briefintroduction;
    }

    public String getFabucontent() {
        return fabucontent;
    }

    public void setFabucontent(String fabucontent) {
        this.fabucontent = fabucontent;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", huatiid=").append(huatiid);
        sb.append(", doctorid=").append(doctorid);
        sb.append(", fabudate=").append(fabudate);
        sb.append(", briefintroduction=").append(briefintroduction);
        sb.append(", fabucontent=").append(fabucontent);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}
package com.linwankaifa.linwancunyi.bean;

import java.io.Serializable;

public class HealthassessmentQuestionBean implements Serializable {
    private static final long serialVersionUID = 1L;
    private Integer questionid;
    private String questioncontent;
    private String optiona;
    private String optionb;
    private String optionc;
    private String optiond;

    public Integer getQuestionid() {
        return questionid;
    }

    public void setQuestionid(Integer questionid) {
        this.questionid = questionid;
    }

    public String getQuestioncontent() {
        return questioncontent;
    }

    public void setQuestioncontent(String questioncontent) {
        this.questioncontent = questioncontent;
    }

    public String getOptiona() {
        return optiona;
    }

    public void setOptiona(String optiona) {
        this.optiona = optiona;
    }

    public String getOptionb() {
        return optionb;
    }

    public void setOptionb(String optionb) {
        this.optionb = optionb;
    }

    public String getOptionc() {
        return optionc;
    }

    public void setOptionc(String optionc) {
        this.optionc = optionc;
    }

    public String getOptiond() {
        return optiond;
    }

    public void setOptiond(String optiond) {
        this.optiond = optiond;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", questionid=").append(questionid);
        sb.append(", questioncontent=").append(questioncontent);
        sb.append(", optiona=").append(optiona);
        sb.append(", optionb=").append(optionb);
        sb.append(", optionc=").append(optionc);
        sb.append(", optiond=").append(optiond);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}
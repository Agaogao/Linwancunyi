package com.linwankaifa.linwancunyi.bean;

import java.io.Serializable;

public class ChatrecordBean implements Serializable {
    private static final long serialVersionUID = 1L;
    private Integer chatid;
    private Integer doctorid;
    private Integer patientid;
    private Integer ispatienttodoctor;
    private String chatcontent;
    private Integer isread;

    public Integer getChatid() {
        return chatid;
    }

    public void setChatid(Integer chatid) {
        this.chatid = chatid;
    }

    public Integer getDoctorid() {
        return doctorid;
    }

    public void setDoctorid(Integer doctorid) {
        this.doctorid = doctorid;
    }

    public Integer getPatientid() {
        return patientid;
    }

    public void setPatientid(Integer patientid) {
        this.patientid = patientid;
    }

    public Integer getIspatienttodoctor() {
        return ispatienttodoctor;
    }

    public void setIspatienttodoctor(Integer ispatienttodoctor) {
        this.ispatienttodoctor = ispatienttodoctor;
    }

    public String getChatcontent() {
        return chatcontent;
    }

    public void setChatcontent(String chatcontent) {
        this.chatcontent = chatcontent;
    }

    public Integer getIsread() {
        return isread;
    }

    public void setIsread(Integer isread) {
        this.isread = isread;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", chatid=").append(chatid);
        sb.append(", doctorid=").append(doctorid);
        sb.append(", patientid=").append(patientid);
        sb.append(", ispatienttodoctor=").append(ispatienttodoctor);
        sb.append(", chatcontent=").append(chatcontent);
        sb.append(", isread=").append(isread);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}
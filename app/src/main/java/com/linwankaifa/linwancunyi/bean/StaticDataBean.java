package com.linwankaifa.linwancunyi.bean;

public class StaticDataBean {
    int PatientNumber;//患者数量
    int patientMaleNumber;//男患者人数
    int patientFemaleNumber;//女患者人数
    int DoctorNumber;//医生数量
    int PhysicianNumber;//医师人数
    int AttendingDoctorNumber;//主治医师人数
    int ChiefPhysicianNumber;//主任医师人数
    int DeputyChiefPhysicianNumber;//副主任医师人数
    int doctorMaleNumber;//男医生数量
    int doctorFemaleNumber;//女医生数量
    int HospitalNumber;//医院数量
    int TopicNumber;//话题数量
    int SypmtonmaticNumber;//症状数量
    int ChatNumber;//聊天数量
    int PatientToDoctorChatNumber;//患者向医生聊天数量
    int DoctorToPatientNumber;//医生向患者聊天数量


    public int getPatientNumber() {
        return PatientNumber;
    }

    public void setPatientNumber(int patientNumber) {
        PatientNumber = patientNumber;
    }

    public int getPatientMaleNumber() {
        return patientMaleNumber;
    }

    public void setPatientMaleNumber(int patientMaleNumber) {
        this.patientMaleNumber = patientMaleNumber;
    }

    public int getPatientFemaleNumber() {
        return patientFemaleNumber;
    }

    public void setPatientFemaleNumber(int patientFemaleNumber) {
        this.patientFemaleNumber = patientFemaleNumber;
    }

    public int getDoctorNumber() {
        return DoctorNumber;
    }

    public void setDoctorNumber(int doctorNumber) {
        DoctorNumber = doctorNumber;
    }

    public int getPhysicianNumber() {
        return PhysicianNumber;
    }

    public void setPhysicianNumber(int physicianNumber) {
        PhysicianNumber = physicianNumber;
    }

    public int getAttendingDoctorNumber() {
        return AttendingDoctorNumber;
    }

    public void setAttendingDoctorNumber(int attendingDoctorNumber) {
        AttendingDoctorNumber = attendingDoctorNumber;
    }

    public int getChiefPhysicianNumber() {
        return ChiefPhysicianNumber;
    }

    public void setChiefPhysicianNumber(int chiefPhysicianNumber) {
        ChiefPhysicianNumber = chiefPhysicianNumber;
    }

    public int getDeputyChiefPhysicianNumber() {
        return DeputyChiefPhysicianNumber;
    }

    public void setDeputyChiefPhysicianNumber(int deputyChiefPhysicianNumber) {
        DeputyChiefPhysicianNumber = deputyChiefPhysicianNumber;
    }

    public int getDoctorMaleNumber() {
        return doctorMaleNumber;
    }

    public void setDoctorMaleNumber(int doctorMaleNumber) {
        this.doctorMaleNumber = doctorMaleNumber;
    }

    public int getDoctorFemaleNumber() {
        return doctorFemaleNumber;
    }

    public void setDoctorFemaleNumber(int doctorFemaleNumber) {
        this.doctorFemaleNumber = doctorFemaleNumber;
    }

    public int getHospitalNumber() {
        return HospitalNumber;
    }

    public void setHospitalNumber(int hospitalNumber) {
        HospitalNumber = hospitalNumber;
    }

    public int getTopicNumber() {
        return TopicNumber;
    }

    public void setTopicNumber(int topicNumber) {
        TopicNumber = topicNumber;
    }

    public int getSypmtonmaticNumber() {
        return SypmtonmaticNumber;
    }

    public void setSypmtonmaticNumber(int sypmtonmaticNumber) {
        SypmtonmaticNumber = sypmtonmaticNumber;
    }

    public int getChatNumber() {
        return ChatNumber;
    }

    public void setChatNumber(int chatNumber) {
        ChatNumber = chatNumber;
    }

    public int getPatientToDoctorChatNumber() {
        return PatientToDoctorChatNumber;
    }

    public void setPatientToDoctorChatNumber(int patientToDoctorChatNumber) {
        PatientToDoctorChatNumber = patientToDoctorChatNumber;
    }

    public int getDoctorToPatientNumber() {
        return DoctorToPatientNumber;
    }

    public void setDoctorToPatientNumber(int doctorToPatientNumber) {
        DoctorToPatientNumber = doctorToPatientNumber;
    }

}

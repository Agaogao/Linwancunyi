package com.linwankaifa.linwancunyi.bean;

import java.io.Serializable;

public class TokenCharactor implements Serializable {
    private static final long serialVersionUID = 1L;
    String token;
    DoctorBean doctor;
    PatientBean patient;
    ManagerBean manager;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public DoctorBean getDoctor() {
        return doctor;
    }

    public void setDoctor(DoctorBean doctor) {
        this.doctor = doctor;
    }

    public PatientBean getPatient() {
        return patient;
    }

    public void setPatient(PatientBean patient) {
        this.patient = patient;
    }

    public ManagerBean getManager() {
        return manager;
    }

    public void setManager(ManagerBean manager) {
        this.manager = manager;
    }


}

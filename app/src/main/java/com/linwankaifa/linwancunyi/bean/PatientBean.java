package com.linwankaifa.linwancunyi.bean;

import java.io.Serializable;

public class PatientBean implements Serializable {
    private static final long serialVersionUID = 1L;
    private Integer patientid;
    private String petname;
    private String patientname;
    private Integer patientage;
    private String patientsex;
    private String birthday;
    private String phone;
    private String accountnumber;
    private String password;
    private Integer height;
    private Integer weight;
    private String ismarriage;
    private String isallergy;
    private String allergens;

    public Integer getPatientid() {
        return patientid;
    }

    public void setPatientid(Integer patientid) {
        this.patientid = patientid;
    }

    public String getPetname() {
        return petname;
    }

    public void setPetname(String petname) {
        this.petname = petname;
    }

    public String getPatientname() {
        return patientname;
    }

    public void setPatientname(String patientname) {
        this.patientname = patientname;
    }

    public Integer getPatientage() {
        return patientage;
    }

    public void setPatientage(Integer patientage) {
        this.patientage = patientage;
    }

    public String getPatientsex() {
        return patientsex;
    }

    public void setPatientsex(String patientsex) {
        this.patientsex = patientsex;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAccountnumber() {
        return accountnumber;
    }

    public void setAccountnumber(String accountnumber) {
        this.accountnumber = accountnumber;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public Integer getWeight() {
        return weight;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }

    public String getIsmarriage() {
        return ismarriage;
    }

    public void setIsmarriage(String ismarriage) {
        this.ismarriage = ismarriage;
    }

    public String getIsallergy() {return isallergy;}

    public void setIsallergy(String isallergy) {this.isallergy = isallergy;}


    public String getAllergens() {
        return allergens;
    }

    public void setAllergens(String allergens) {
        this.allergens = allergens;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", patientid=").append(patientid);
        sb.append(", petname=").append(petname);
        sb.append(", patientname=").append(patientname);
        sb.append(", patientage=").append(patientage);
        sb.append(", patientsex=").append(patientsex);
        sb.append(", birthday=").append(birthday);
        sb.append(", phone=").append(phone);
        sb.append(", accountnumber=").append(accountnumber);
        sb.append(", password=").append(password);
        sb.append(", height=").append(height);
        sb.append(", weight=").append(weight);
        sb.append(", ismarriage=").append(ismarriage);
        sb.append(", isallergy=").append(isallergy);
        sb.append(", allergens=").append(allergens);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}
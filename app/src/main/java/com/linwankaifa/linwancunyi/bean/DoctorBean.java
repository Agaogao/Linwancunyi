package com.linwankaifa.linwancunyi.bean;

import java.io.Serializable;

public class DoctorBean implements Serializable {
    private static final long serialVersionUID = 1L;
    private Integer doctorid;
    private Integer hospitalid;
    private String doctorname;
    private Integer doctorage;
    private String doctorsex;
    private String department;
    private String position;
    private String speciality;
    private String doctoraccountnumber;
    private String doctorpassword;

    public Integer getDoctorid() {
        return doctorid;
    }

    public void setDoctorid(Integer doctorid) {
        this.doctorid = doctorid;
    }

    public Integer getHospitalid() {
        return hospitalid;
    }

    public void setHospitalid(Integer hospitalid) {
        this.hospitalid = hospitalid;
    }

    public String getDoctorname() {
        return doctorname;
    }

    public void setDoctorname(String doctorname) {
        this.doctorname = doctorname;
    }

    public Integer getDoctorage() {
        return doctorage;
    }

    public void setDoctorage(Integer doctorage) {
        this.doctorage = doctorage;
    }

    public String getDoctorsex() {
        return doctorsex;
    }

    public void setDoctorsex(String doctorsex) {
        this.doctorsex = doctorsex;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getSpeciality() {
        return speciality;
    }

    public void setSpeciality(String speciality) {
        this.speciality = speciality;
    }

    public String getDoctoraccountnumber() {
        return doctoraccountnumber;
    }

    public void setDoctoraccountnumber(String doctoraccountnumber) {
        this.doctoraccountnumber = doctoraccountnumber;
    }

    public String getDoctorpassword() {
        return doctorpassword;
    }

    public void setDoctorpassword(String doctorpassword) {
        this.doctorpassword = doctorpassword;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", doctorid=").append(doctorid);
        sb.append(", hospitalid=").append(hospitalid);
        sb.append(", doctorname=").append(doctorname);
        sb.append(", doctorage=").append(doctorage);
        sb.append(", doctorsex=").append(doctorsex);
        sb.append(", department=").append(department);
        sb.append(", position=").append(position);
        sb.append(", speciality=").append(speciality);
        sb.append(", doctoraccountnumber=").append(doctoraccountnumber);
        sb.append(", doctorpassword=").append(doctorpassword);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}